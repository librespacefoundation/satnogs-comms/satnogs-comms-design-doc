%% SatNOGS COMMS - Environmental Test Report
%% Copyright (C) 2024 Libre Space Foundation
%%
%% This work is licensed under a
%% Creative Commons Attribution-ShareAlike 4.0 International License.
%%
%% You should have received a copy of the license along with this
%% work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.

\documentclass[english,title,a4paper]{report}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{float}
\usepackage[hidelinks]{hyperref}
\usepackage[toc,acronyms,section,nopostdot,nonumberlist,section=section,numberedsection]{glossaries}
\usepackage[toc,page]{appendix}
\usepackage{graphicx}
\usepackage{titlesec}
\usepackage{amssymb}
\usepackage{textcomp,gensymb}
\usepackage{booktabs}
\usepackage{pifont}
\usepackage{verbatim}
\usepackage{rotating}
\usepackage{makecell}
\usepackage{lipsum}
\usepackage{longtable}
% \usepackage[paper=portrait,pagesize]{typearea}
\usepackage{multirow}
\usepackage{listings}
\usepackage{subfig}
\usepackage{fancyhdr}
\usepackage{lastpage}
\usepackage{pdfpages}

\newcommand\TBD[1]{\textcolor{red}{#1}}

\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=black,
    filecolor=magenta,
    urlcolor=blue,
    citecolor=black,
}

\fancypagestyle{plain}{
  \fancyhf{}
  \fancyhead[L]{\includegraphics[width=1.5cm]{diagrams/LSF_HD_Icon_Color.png}} %chktex-file 25
  \fancyhead[R]{SatNOGS COMMS\\EMC Test Report}
  \fancyfoot[L]{Version \version}
  \fancyfoot[C]{\thepage{} of~\pageref{LastPage}}
  \fancyfoot[R]{\copyright{} Libre Space Foundation}
  \renewcommand{\headrulewidth}{0.4pt}
  \renewcommand{\footrulewidth}{0.4pt}
}
\pagestyle{plain}
\setcounter{secnumdepth}{3}
\setcounter{tocdepth}{3}

\input{include/version}

\makeglossaries{}

%% Acronyms
\input{glossaries/acronyms}

\title{
  EMC Test Report \\
  \large{SatNOGS COMMS}
}
\author{\includegraphics[scale=0.2]{diagrams/lsf-logo.png}}
\date{Version \version}

\titleformat{\chapter}[hang]
{\normalfont\bfseries\Huge}{\thechapter.}{10pt}{}

\begin{document}

\hypersetup{pageanchor=false}
\maketitle
\hypersetup{pageanchor=true}
\tableofcontents

\chapter{Changelog}

\begin{tabular}{llp{6.5cm}}
    \toprule
    Version & Date          & Changes                                       \\ \midrule
    1.5     & Dec 10, 2024  & Initial release                               \\ \midrule
    1.5.1   & Dec 10, 2024  & Fix static GitLab pipeline                    \\ \midrule
    1.5.2   & Dec 18, 2024  & No Changes                                    \\ \bottomrule
\end{tabular}

\newpage
\printglossary[type=\acronymtype]

\chapter{Introduction}

This document outlines the required equipment, setup, and test procedures, as well as the results obtained, for the \acrshort{emc} tests conducted on the SatNOGS COMMS Board to qualification levels at the High Voltage Laboratory of the School of Electrical and Computer Engineering, National Technical University of Athens.

\chapter{Packing List}

Listed below is the equipment one must bring to the test site.
User has the option of pre-assembling some parts to ease the on-site preparations, nevertheless the list is provided in a fragmented fashion.
Minimum quantities are listed and the user can pack more according to their judgment.

\section{Boards and Devices}

\begin{itemize}
    \item SatNOGS COMMS board v0.3.1 (\acrshort{dut})
    \item PC/104 Breakout board (any version)
    \item An assembled LibreCube plate
    \item Segger-J-link debuger with cables and adapter for SatNOGS-COMMS and PC
    \item JTAG-HS3 programmer with cables and adapter for SatNOGS-COMMS and PC
    \item USB to CAN-FD adapter, candleLight FD, with cables for SatNOGS-COMMS and PC
    \item USB to UART adapter, with cables for SatNOGS-COMMS and PC
    \item Sony Cyber-shot DSC-RX100, digital camera
    \item USRP B210, with cable for PC
    \item RF Attenuators, terminators and adapters, \autoref{tab:cable-losses}
    \item (2$\times$) Crocodile Clip Socket 4mm-Red and black
    \item 850--6500 MHz Log Periodic PCB antenna, \url{https://www.wa5vjb.com/pcb-pdfs/LP8565A2.pdf}
    \item 400--435 MHz dipole PHASMA antenna, \url{https://gitlab.com/librespacefoundation/phasma/phasma-ant-hdrm-electronics}
    \item omnidirectional antennas for USRP for UHF and for S-Band
\end{itemize}

\section{Cables and RF Parts}

\begin{itemize}
    \item (2$\times$) Male MMCX to female SMA cable, \autoref{tab:cable-losses}.
    \item (4$\times$) Male SMA to male SMA cable, \autoref{tab:cable-losses}.
    \item (4$\times$) Female SMA to Female adapter, \autoref{tab:cable-losses}.
    \item DF13 Hirose connector to (2$\times$) banana Terminals 4mm male (+12V, GND), 26AWG, Lenght 300mm.
    \item Picoblade Molex connector to D-SUB 9 female, 26AWG, Length 1200mm
\end{itemize}

Some more specific information about the RF cables, terminators and attenuators used are provided in Table~\ref{tab:cable-losses}.

\begin{table}[H]
\centering
\begin{tabular}{|c|c|c|} % chktex-file 44
\hline
Component                  & Model                         & Comment                                                                                          \\ \hline
Attenuators                & \begin{tabular}[c]{c}Mini-circuit\\$BW-SxW2+$\end{tabular}       & \begin{tabular}[c]{@{}c@{}}2$\times$40,3,6,10,20,2$\times$30 dB\\ Rated at 1W\end{tabular}               \\ \hline
\begin{tabular}[c]{@{}c@{}}Adapter Female SMA\\ to Female SMA\end{tabular} & NA               & \begin{tabular}[c]{@{}c@{}}Measured -0.1dB at 400 MHz\\ Measured -0.1dB at 2400 MHz\end{tabular}  \\ \hline
Terminators 50 Ohm         & \begin{tabular}[c]{c}Mini-circuit\\$MCL ANNE - 50+$\end{tabular} & Rated at 1W                                                                                      \\ \hline
\begin{tabular}[c]{@{}c@{}}male SMA to\\male SMA cable, 600mm\end{tabular} & \begin{tabular}[c]{c}Mini-circuit\\$FL086-248M$\end{tabular}  & \begin{tabular}[c]{@{}c@{}}Measured -0.3dB at 400 MHz\\ Measured -1.2dB at 2400 MHz\end{tabular} \\ \hline % chktex-file 8
\begin{tabular}[c]{@{}c@{}}male MMCX to\\female SMA cable, 152.4mm\end{tabular}& Molex $897616820$               & \begin{tabular}[c]{@{}c@{}}Measured -0.5dB at 400 MHz\\ Measured -1dB at 2400 MHz\end{tabular}   \\ \hline
\end{tabular}
\caption{RF hardware information}\label{tab:cable-losses}
\end{table}

\subsection{Device under Test}

All the set up in \autoref{fig:dut}.
For all connections follow user manual.

\begin{figure}
  \centering
  \includegraphics[width=1.0\textwidth]{diagrams/satnogs-comms-user-manual-setup.jpg}
  \caption{Device under Test}\label{fig:dut}
\end{figure}

\chapter{Software and Logs}

To ensure firmware traceability, the two following actions must be performed.
\begin{enumerate}
    \item Record by commit or release the software that is run in \acrshort{mcu} of SatNOGS COMMS
    \item Record by commit or release the software that is run in \acrshort{fpga} of SatNOGS COMMS
    \item Log all the commands that are send though CAN to SatNOGS COMMS and the results with a UTC timestamp
\end{enumerate}

\begin{table}[H]
\begin{tabular}{|c|m{6cm}|}
\hline
\textbf{Test Date}               & 20 to 22 November 2024 \\ \hline
\textbf{Testers}                 & SatNOGS COMMS Team     \\ \hline
\textbf{MCU SW Commit Hash}      & 
\begin{itemize}
    \item \href{https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-software-mcu/-/releases/emc-radiated}{Radiated emissions test custom firmware}
    \item \href{https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-software-mcu/-/commit/e145018e3c59e4145572aa7b48a58ba282eb5c84}{Immunity tests firmware}
    \item \href{https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-yamcs/-/commit/af50d7520eccc1eefe14df445fe64cc4cc4e3a92}{YAMCS control utility}
\end{itemize}
 \\ \hline
\textbf{FPGA Config Commit Hash} & \href{https://xilinx-wiki.atlassian.net/wiki/spaces/A/pages/18842141/FreeRTOS}{FreeRTOS Hello World application} \\ \hline
\textbf{LSF Cloud Link for logs} & \href{https://cloud.libre.space/f/674393}{Logs Directory}\\ \hline
\end{tabular}
\end{table}

\chapter{EMC tests}

In duration of \acrshort{emc} tests 2 modes of operations are selected according to ECSS-E-ST-20--07C Rev. 1,

\begin{itemize}
    \item During emission measurements, the \acrshort{dut} shall be placed in the operating mode, which produces maximum emissions.
    \item During susceptibility testing, the \acrshort{dut} shall be placed in its most susceptible operating mode.
\end{itemize}

\section{Emission tests}
The laboratory provides the below emission tests:
\begin{itemize}
    \item MIL-STD-461 CE102: Alternative method EN 61000--6--4: 10kHz-10MHz, with modifications as described on appendix \autoref{app:test-relts}.
    \item MIL-STD-461 RE102: Alternative method EN 61000--6--4: 10kHz-18GHz, with modifications as described on appendix \autoref{app:test-relts}.
\end{itemize}

During these tests SatNOGS COMMS (\acrshort{dut}) is running a stress procedure continuously, consisting from multiple Zephry-RTOS tasks.
The procedure is the following:
\begin{itemize}
    \item The UHF task transmits 100 FSK-2 frames at 435 MHz, with a delay between them of 100ms. Their bitrate is 50 kbps. 
    The transmission power is set to +20 dBm by setting the AT86RF215 PA power register to 18.  When this burst finishes, the task configures the UHF interface into RX mode at the same frequency.
    
    \item The S-Band task starts the transmission, right after the UHF interface enters RX mode. 
    It transmits 100 MSK frames  at 2250 MHz, with a delay between them of 100ms. Their bitrate is 400 kbps.
    The transmission power is set to 24 dBm by setting the AT86RF215 PA power register to 18.
    The local oscillator frequency of the RF mixer is set to 216 MHz and the intermediate frequency at the AT86RF215 is set at 2466 MHz.
    When this burst finishes, the task configures the S-band interface into RX mode at 2050 MHz.
    The local oscillator frequency of the RF mixer is set to 390 MHz and the intermediate frequency at the AT86RF215 is set at 2440 MHz.
    
    \item The eMMC management task, performs a write/read-back test on the eMMC right after the S-Band interface enters RX mode.
    The test writes 1000 sectors, each one 512 byte long and tries to read back the data written.
    The clock between the MCU and the eMMC operates at 64 MHz.
    With this, done the procedure starts again with the UHF task transmission and repeats for the entire duration of the test, without interruption.
\end{itemize}
Concurrently, during boot the test firmware powers-up the FPGA which is running a FreeRTOS application on one of the generic ARM cores of the ZYNQ-PS.
The application executes a stress test, occupying fully the CPU.

During the procedure described before, no communication between the ground station via \acrshort{rf} or CAN Bus is performed in an effort to avoid any noise source from external systems.

\section{Immunity tests}
The laboratory provides the below immunity tests:
\begin{itemize}
    \item MIL-STD-461 CS101: Alternative method only for common mode disturbance: EN 61000--4--16 at test level 3 (dc-150kHz at 10Vrms maximum), for power input port, with modifications as described on appendix \autoref{app:test-relts}.
    \item MIL-STD-461 CS114: Alternative method EN 61000--4--6 at test level 3Vrms (150kHz-200MHZ at 29V maximum), for power input port and CAN Bus port, with modifications as described on appendix \autoref{app:test-relts}.
    \item MIL-STD-461 RS103: Alternative method EN 61000--4--3 at test level 2 (80MHz-6GHz at $3V/m$ maximum), with modifications as described on appendix \autoref{app:test-relts}.
\end{itemize}

The limit selection for radiated immunity was done according to ECSS-E-ST-20--07C Rev. 1 appendix A.14, as SatNOGS COMMS operates inside the main frame of a satellite.
The limit is 1 V/m but a field strength of 3V/m was applied.
In the same manner according to section 5.2.10.1 of ECSS-E-ST-20--07C Rev. 1 the Dwell time is selected to 1s as the response time of \acrshort{dut} is less that 500ms.
The response time is defined as the time that SatNOGS COMMS needs to respond to commands either on CAN Bus or on \acrshort{rf}. 

During these tests SatNOGS COMMS (\acrshort{dut}) is running the default firmware, as it should run in a normal space mission.
The UHF interface is active and receiving frames at 435 MHz, with FSK-2 modulation at 50 kbps.

For each test run, a \acrshort{ber} test is conducted using the corresponding GNU Radio low-graph and USRP B210 SDR.
The signal level, has is adjusted so the \acrshort{dut} receives a signal of -86 dBm.
To achieve this, the USRP B210 is configured with a TX gain setting of 66 dB and 60 dB attenuation is introduced between the USRP and the \acrshort{dut} using two attenuators (40 and 20 dB).
Before each test, the \acrshort{dut} is instructed to reset the received frames counter.
After each test, this counter is retrieved so the \acrshort{fer} as well as the \acrshort{ber} can be calculated. 

In addition, during each test the \acrshort{dut} is instructed through CAN Bus telecommand to perform a write/read-back test at the eMMC,
to evaluate the data integrity at the persistent storage.
The results of this test, along with additional telemetry (uptime, power) is periodically fetched and logged to ensure proper operation throughout the entire immunity test duration.

The Performance criteria for the immunity test are:
\begin{itemize}
    \item The bit error rate (BER) of the received signal should not exceed 0.05 (5\%)
    \item The data integrity of eMMC read/write operations should not exceed 1\% error rate
\end{itemize}

\chapter{Conclusion}

As depicted by the conducted emissions results, a surpassing of the limit, by around 15 dB, appears at a frequency of approximately 2.18 MHz. By reviewing the available information on the operation of the individual devices present on the SatNOGS COMMS board, it can be derived that the above frequency is connected to the operation of the power supply with P/N of TPS62913, which is used in version v0.3.1. 
This power supply operates at a 2.2 MHz switching frequency when tuned by a 18.2 kOhm resistor.
On the board, an 18.2 kOhm resistor of 1\% with P/N of CRCW040218K2FKED is used.
The small deviation of approximately 20 kHz, could be explained by the lower accuracy of the EMI receiver when configured to analyze a broader frequency range.
In a frequency range adjacent to the frequency of interest, a more accurate result would appear.
In addition, harmonics are present at multiples of the fundamental frequency, which are below the limit.
Furthermore, a small peak is detected at slightly more than 1 MHz which is, at present, of unknown origin and requires further investigation.

Regarding the radiated emissions results, it is apparent that the harmonics of the above frequency are present on almost the entire frequency range.
They are most likely radiated by an untwisted part of the power cable which extends at roughly 15 cm.
The preponderance of this phenomenon is mostly exhibited at around 500 MHz, which could be related the length of the untwisted power cable length, as the wavelength of 500 MHz is approximately 60 cm.
This implies that the untwisted power cable strongly radiates at 500 MHz, because its length is at the quarter wavelength ($\lambda$/4).
A related photo can be found at \url{https://cloud.libre.space/s/oztK96DoH6WqZTX}.
Generally, these emissions do not exceed the limit except for a small region at around 480 MHz (471 to 504 MHz, at the corresponding results table).
In addition, there is an intentional emission, due to the operating condition of the board, at 435 MHz which occurs at 30 second intervals.
Specifically, as a result of the shape of the emission mask, peaks around 435 MHz surpassing 90 dBuV/m can be safely considered part of the intentional emission.
Unfortunately, the origin of the adjacent peaks that seem to not be part of the emission mask, remains unclear as they could not be evaluated from the EMI receiver and would probably require a real-time observation to at least verify if they are residues of the intentional emission or related to the power supply operating frequency.
Moreover, there exist peaks highly related to the operation of the S-Band RF mixer. In particular, the combination of the Local Oscillator (LO) frequency to the Intermediate Frequency (IF) reveals spurious emissions very close to some peaks' frequencies.
For instance, the peaks around 520 MHz could be related to the combination of -9 LO and 1st IF which is calculated at 520 MHz.
Another point is that these spurious emissions occurred intermittently, validating the above hypothesis of their origin.
Additionally, they only occurred when the S-Band radio was utilized in the operation of the device under test (DUT).
On the other hand, the harmonics of the 2.18 MHz were constantly present.

It is important to state that the measurements were made by using a peak detector. On the other hand, using a quasi-peak detector, would eliminate emissions of periodic nature, such as mixer spurious or intentional ones. Moreover, even if limits from other standards are used, for example the radiated emissions limit from ECSS-E-ST-20--07C by converting it to 3m distance, of the measurements test setup, the results still do not pass.

Concerning the radiated susceptibility results, no significant performance degradation occurred on the radio performance (<0.1\% FER), 0 error bits on the eMMC and no abnormal resets on the DUT.
The exact same results also apply to all the conducted susceptibility test cases.
This concludes that the DUT is immune to an electric field of 3V/m at 3 meters and frequency range of 80 MHz to 1 GHz and 3V/m at 2 meters and frequency range of 1 GHz to 6 GHz.
It is also immune to 3V\textsubscript{rms} directly to the power and CAN bus ports at 0.15 MHz to 80 MHz and 10 V\textsubscript{rms} at 15 Hz to 150 kHz.


\begin{appendices}

\chapter{Test Results}\label{app:test-relts}

\includepdf[pages=-]{diagrams/10279_Libre Space Foundation_EMC.pdf} % chktex-file 25

\end{appendices}

\end{document}