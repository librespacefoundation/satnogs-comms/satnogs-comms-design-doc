% SatNOGS COMMS System Design Document - Verification plan
% Copyright (C) 2020, 2021, 2022, 2023, 2024 Libre Space Foundation
%
% This work is licensed under a
% Creative Commons Attribution-ShareAlike 4.0 International License.
%
% You should have received a copy of the license along with this
% work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.

% Linter settings
% chktex-file -2
% chktex-file 35

\documentclass[english,title,a4paper]{report}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{float}
\usepackage[hidelinks]{hyperref}
\usepackage[toc,acronyms,section,nopostdot,nonumberlist,section=section,numberedsection]{glossaries}
\usepackage[toc,page]{appendix}
\usepackage{graphicx}
\usepackage{titlesec}
\usepackage{amssymb}
\usepackage{textcomp,gensymb}
\usepackage{booktabs}
\usepackage{pifont}
\usepackage{verbatim}
\usepackage{fancyhdr}
\usepackage{lastpage}
\usepackage{rotating}
\usepackage{makecell}
\usepackage{lipsum}
\usepackage{longtable}
\usepackage{rotating}
\usepackage{indentfirst}
\usepackage{multirow}
\usepackage{array}
\usepackage[yyyymmdd]{datetime}
\renewcommand{\dateseparator}{-}
\newcolumntype{C}[1]{>{\centering\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}

\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=black,
    filecolor=magenta,
    urlcolor=blue,
    citecolor=black,
}

\fancypagestyle{plain}{
  \fancyhf{}
  \fancyhead[R]{SatNOGS COMMS\\Verification and Test Plan}
  \fancyhead[L]{\includegraphics[width=1.5cm]{diagrams/LSF_HD_Icon_Color.png}} % chktex-file 25
  \fancyfoot[L]{Version \version}
  \fancyfoot[C]{\thepage{} of~\pageref{LastPage}}
  \fancyfoot[R]{\copyright{} Libre Space Foundation}
  \renewcommand{\headrulewidth}{0.4pt}
  \renewcommand{\footrulewidth}{0.4pt}
}

\pagestyle{plain}
\setcounter{secnumdepth}{3}
\setcounter{tocdepth}{3}

\newcommand{\cmark}{\ding{51}}%
\newcommand{\xmark}{\ding{55}}%

\input{include/version}
\input{include/requirements-urls}
\input{include/test-cases-urls}
\input{include/replacements.tex}

\makeglossaries{}

%% Acronyms
\input{glossaries/acronyms}

\title{Verification and Test Plan \\\large{SatNOGS COMMS}}
\author{\includegraphics[scale=0.2]{diagrams/lsf-logo.png}}
\date{Version \version}

\titleformat{\chapter}[hang]
{\normalfont\bfseries\Huge}{\thechapter.}{10pt}{}

\begin{document}

\hypersetup{pageanchor=false}
\maketitle
\hypersetup{pageanchor=true}
\tableofcontents

\chapter{Changelog}

\begin{tabular}{llp{6.5cm}}
  \toprule
  Version & Date               & Changes                                             \\
  \midrule
  1.0     & Dec 18, 2023       & \textbullet{} Add change-log \newline
                               \textbullet{} Add a sentence in section 1.1 to connect this document with Test Readiness Document  \\ \midrule
  1.1     & Dec 29, 2023       & No changes \\ \midrule
  1.2     & Mar 19, 2024       & No changes \\ \midrule
  1.3     & Apr 30, 2024       & No changes \\ \midrule
  1.4     & Nov 05, 2024       & No changes \\ \midrule
  1.5     & Dec 10, 2024       & No changes \\ \midrule
  1.5.1   & Dec 10, 2024       & Fix static GitLab pipeline \\ \midrule
  1.5.2   & Dec 18, 2024       & No changes     \\ \bottomrule
\end{tabular}

\newpage
\printglossary[type=\acronymtype]

\chapter{Introduction}

\section{Purpose and Scope}

This document describes verification and test procedures which verify that implementation of `SatNOGS-COMMS' complies with the defined System Requirement Specification.
It includes verification plan, test plan and inspection test cases at Appendix~\ref{appendix:inspection}.
The verification test cases as well as facilities and deviations are part of Test Readiness Document.



\chapter{Product Presentation}

The SatNOGS COMMS is a turnkey solution enabling robust and reliable communication for \acrshort{leo} Cubesats.
It operates in the \acrshort{uhf} and S frequency bands, providing uplink and downlink capabilities on both of these spectrum regions.
Demodulation and decoding of telemetry and payload data produced by SatNOGS COMMS is fully compatible with SatNOGS ground stations.
SatNOGS COMMS also integrates tightly to SatNOGS Network, supporting a mission control system for \acrshort{tcnc} and real-time dashboards.
The SatNOGS COMMS architecture consists of two major modules, the space and the ground segment.
The space segment consists of the hardware and the corresponding software that controls it, while the ground segment is used for uplink and downlink.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.65\textwidth]{diagrams/srr-system-components.pdf}
  \caption{SatNOGS COMMS components}\label{fig:system-components}
\end{figure}

\chapter{Verification Plan}

\section{Verification approach}

\subsection{Verification Methods}

Verification is accomplished with at least one of the methods bellow depending on schedule and cost constraints:
\begin{itemize}
\item Inspection:\\
  Verification by inspection consists of visual determination of physical characteristics, for example constructional features, hardware conformance to document drawing or workmanship requirements, physical conditions, software source code conformance with coding standards etc.
\item Review of design:\\
  Verification by \acrshort{rod} consists of using approved records or evidence that unambiguously show that the requirement is met.
\item Analysis:\\
  Verification by analysis consists of performing theoretical or empirical evaluation using techniques, such as systematic, statistical and
  qualitative design analysis, modeling and computational simulation etc.
\item Test:\\
  Verification by test consists of measuring product performance and functions under representative simulated environments.
\end{itemize}

\subsection{Verification Levels}

Requirements are verified in three different verification levels:

\begin{itemize}
\item System Level: The SatNOGS COMMS end-to-end system.
\item Segment level: The ground and space segments.
\item Equipment/Unit Level: The components of ground and space segments.
\end{itemize}

\section{Model philosophy}

The Model philosophy that is selected for SatNOGS COMMS is the hybrid model philosophy.
The objective of following this approach is the reduction of risks while maintaining a balance in schedule and cost.

The output of this activity is an \acrfull{eqm} for a new communication subsystem. By having a new design with \acrshort{cots} hardware and by following an agile development, the models needed for each verification level of SatNOGS COMMS are described bellow (\textit{Note: Some of the models, that will be used in different verification levels, may be identical/represented by the same hardware}):

\begin{itemize}
\item System Level
  \begin{itemize}
  \item \acrfull{dm}
  \item \acrfull{efm}
  \item \acrfull{stm}
  \item \acrfull{eqm}
  \item Suitcase Model
  \end{itemize}

\item Segment level
  \begin{itemize}
  \item \acrfull{dm}
  \item \acrfull{efm}
  \item \acrfull{eqm}
  \item Ground Segment Models
    \begin{itemize}
    \item Model for developing and verifying procedures and the \acrfull{hmi}
    \item Model for \acrshort{tcnc} data processing and associated procedures
    \end{itemize}
  \end{itemize}

\item Equipment/Unit Level
  \begin{itemize}
  \item \acrfull{mu}
  \item \acrfull{dm}

  \end{itemize}

\end{itemize}

\section{Verification strategy}

Verification strategy of SatNOGS COMMS is a combination of the different verification methods, giving emphasis on test method, at the different verification levels as these described on the previous section.
Following this strategy is the way to verify, test and fulfill requirements defined in System Requirement Specification and the corresponding GitLab requirement issues.

\section{Verification control methodology}

Monitoring and controlling the verification procedures is done through the issue tracker of open source git-repository manager GitLab by tracking verification issues for each method is applied at each level.
GitLab issue tracker offers organizing and managing tools in order to effectively monitor and control the verification process.

\chapter{Test Plan}

Verification by test is performed at all three verification levels of the SatNOGS COMMS system in order to ensure that System Requirement Specification is satisfied.

\section{Assignment of Responsibilities}
Below are the roles that will be involved in the SatNOGS COMMS test campaign and their responsibilities:

\begin{itemize}
    \item \textbf{Testers} will be responsible to perform and operate the test process of SatNOGS COMMS\@.
    \item \textbf{QA and Test Manager} will be responsible for the quality assurance and safety of the test process.
    \item \textbf{\acrshort{lsf} Test Facility Manager} will be responsible to ensure \acrshort{lsf} test facility readiness before and during the test process.
    \item \textbf{External Facility Personnel} will be responsible to ensure external facilities' readiness before and during the test process.
\end{itemize}

\section{Levels}

There are three different levels of testing:

\subsubsection{Unit Testing}

The purpose of this level is to test the smallest units of implementation and facilitate the development process.
Unit tests are implemented and executed by the developer.
There is no mapping or traceability of unit tests to system requirements.
Unit tests are executed automatically to generate test coverage reports, that are used as feedback for proof of testing, risk assessment and acceptance of implementation by the Test Manager.

\subsubsection{Functional/Module Testing}

The purpose of this level is to verify correct implementation of low-level functional requirements for each of ground and space segment.
Functional tests are implemented by the developer and executed by the tester.
Mapping and traceability of functional tests to system requirements is provided indirectly through association of low-level functional requirements to high-level system requirements.
Functional tests are executed automatically and generate pass/fail reports, that are used as feedback for proof of testing, risk assessment and acceptance of implementation by the Test Manager.

\subsubsection{Integration/End-to-end Testing}

The purpose of this level is to verify correct implementation of high-level system requirements.
\acrshort{e2e} tests are implemented and executed by the tester.
There are direct mapping and traceability of \acrshort{e2e} tests to system requirements.
\acrshort{e2e} tests are executed automatically and generate pass/fail reports, that are used as feedback for proof of testing, risk assessment and acceptance of implementation by the Test Manager.

\begin{appendices}

\chapter{Verification by Inspection}\label{appendix:inspection}

  \IfFileExists{include/inspection-cases}{\input{include/inspection-cases}}{} % chktex 27

  \section{Documentation}\label{sec:documentation}
  \ifdefined\DocTest{}
  \DocTest{}
  \fi
  \clearpage

  \section{Interface}\label{sec:interface}
  \ifdefined\Interface{}
  \Interface{}
  \fi
  \clearpage

  \section{Functional}\label{sec:functional}
  \ifdefined\Functional{}
  \Functional{}
  \fi
  \clearpage

  \section{Performance}\label{sec:performance}
  \ifdefined\Performance{}
  \Performance{}
  \fi
  \clearpage

  \section{EMC}\label{sec:EMC}
  \ifdefined\EMC{}
  \EMC{}
  \fi
  \clearpage

  \section{Environmental}\label{sec:environmental}
  \ifdefined\Environmental{}
  \Environmental{}
  \fi
  \clearpage

  \section{End To End (E2E)}\label{sec:e2e}
  \ifdefined\EndToEnd{}
  \EndToEnd{}
  \fi
  \clearpage

  \section{Non Tested Requirements}
  \subsubsection{Title}
  Non Tested Requirements
  \subsubsection{Description}
  Non Tested Requirements
  \subsubsection{Type}
  Non Tested Requirements
  \subsubsection{Requirements}
  \begin{itemize}
  \item \req{30}
  \item \req{31}
  \item \req{34}
  \item \req{36}
  \item \req{58}
  \item \req{38}
  \item \req{86}
  \item \req{87}
  \item \req{88}
  \item \req{89}
  \item \req{90}
  \item \req{91}
  \item \req{92}
  \item \req{51}
  \end{itemize}
  \newpage

\end{appendices}

\end{document}
