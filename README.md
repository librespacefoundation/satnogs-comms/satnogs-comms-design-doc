# SatNOGS COMMS System Design Document #

SatNOGS COMMS System Design Document.

## Rendered documents ##

#### Latest GitLab Tag:
* [Tags](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-design-doc/-/tags/)

#### Latest builds:
* [System Requirements](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-design-doc/-/jobs/artifacts/master/raw/build/system-requirements.pdf?job=docs)
* [Design and Development Plan](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-design-doc/-/jobs/artifacts/master/raw/build/design-development-plan.pdf?job=docs)
* [System Design](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-design-doc/-/jobs/artifacts/master/raw/build/system-design.pdf?job=docs)
* [Thermal analysis](https://cloud.libre.space/s/rFbpTQc7Zw6XYec)
* [Mechanical analysis](https://cloud.libre.space/s/f5xR4wWn8AJfgAt)
* [FMEA and FDIR](https://cloud.libre.space/s/xzskpy8m3Nb54YL), based on [AcubeSAT - FMEA](https://gitlab.com/acubesat/systems-engineering/fmea)
* [Quality Assurance Plan](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-design-doc/-/jobs/artifacts/master/raw/build/quality-assurance-plan.pdf?job=docs)
* [Verification Plan](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-design-doc/-/jobs/artifacts/master/raw/build/verification-plan.pdf?job=docs)
* [Test Readiness Document](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-design-doc/-/jobs/artifacts/master/raw/build/test-readiness-document.pdf?job=docs)
* [Test Report](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-design-doc/-/jobs/artifacts/master/raw/build/test-report.pdf?job=docs)
* [Environmental Test Report](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-design-doc/-/jobs/artifacts/master/raw/build/environmental-test-report.pdf?job=docs)
* [EMC Test Report](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-design-doc/-/jobs/artifacts/master/raw/build/emc-test-report.pdf?job=docs)
* [Environmental Test](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-design-doc/-/jobs/artifacts/master/raw/build/environmental-test.pdf?job=docs)
* [Final Report](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-design-doc/-/jobs/artifacts/master/raw/build/final-report.pdf?job=docs)
* [ICD](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-design-doc/-/jobs/artifacts/master/raw/build/ICD.pdf?job=docs)
* [User Manual](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-design-doc/-/jobs/artifacts/master/raw/build/user-manual.pdf?job=docs)
* [Option Sheet](https://docs.google.com/document/d/1SKXAWAFKz_AL8DvUsES3PH7ompJgqjFlwxF_gHHOcQk/edit?usp=sharing)
* [Product Flyer](https://docs.google.com/document/d/1QOcbxB9N_Mb0yUyeSQQ9H0zeVXtukuSRbpYqxWod0XE/edit?usp=sharing)


## End-User documentation

From the list above, a reduced selection of documents that contain all necessary information for an end-user has been compiled.
The other documents serve either as developer documentation and provide deeper analysis into the various aspects of the design (e.g. the System Design document), or might be required by the end-user at a later stage of their design or for assurance purposes (e.g. Test Report document).
The necessary documentation for and end-user consists of the :
* Product Flyer
* Option sheet
* ICD
* User Manual


 ## Versioning ##

When a document is released (for example is send to ESA), before this the [version](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-design-doc/-/blob/master/include/version.tex?ref_type=heads) must be updated.
All the documents are released all together because are "leaved" under the same repository.
For that reason it is important to update all changelog tables form all documents.
If there no any change to a specific document, just add a No change comment.
All documents are released under the: [GitLab - Tags](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-design-doc/-/tags).
Notes:
* Until version 0.7 we didn't use GitLab - Tags but Libre Space Foundation Cloud and Overleaf Labels.
* For version 1.1 and above we use GitLab - Tags.

The semantic versioning is [MAJOR.MINOR](https://semverdoc.org/). That means:
* MAJOR version when your document has undergone significant changes,
* MINOR version when new information has been added to the document or information has been removed from the document.

## Rendering ##

### Requirements ###

- TeX Live (full) >= 2022
- CMake >= 2.8
- Python >= 3.6
- python-gitlab >= 3.6.0
- pandoc >= 2.17.1.1
- pypandoc >= 1.8
- Jinja2 >= 3.0.0
- chktex >= 1.7.6

### Checking-out source ###

To check-out the source code:

```
git clone https://git.overleaf.com/5e66a25b25dcf00001fc197e satnogs-comms-design-doc
cd satnogs-comms-design-doc
```

### Building ###

To render the documents in PDF format:

```
mkdir build
cd build
cmake ..
make pdf
```

### Linting ###

To lint the source code of the documents:

```
chktex *.tex
```

### Test Case - labels ###

Use this

```
\label{tc:1a}
```

in [test-report.tex](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-design-doc/-/blob/master/test-report.tex?ref_type=heads) to mention a specific test case. In case of multiple
mentions use:

```
\label{tc:1n}
```

where n= a, ..., j.

### Requirements - labels ###

Use this

```
\label{req:1a}
```

in [system-design.tex](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-design-doc/-/blob/master/system-design.tex?ref_type=heads) to mention a specific requirement. In case of multiple
mentions use:

```
\label{req:1n}
```

where n= a, ..., j.

## License ##

[![license](https://img.shields.io/badge/license-CC%20BY--SA%204.0-6672D8.svg)](LICENSE)
[![Libre Space Foundation](https://img.shields.io/badge/%C2%A9%202020--2024-Libre%20Space%20Foundation-6672D8.svg)](https://libre.space/)
