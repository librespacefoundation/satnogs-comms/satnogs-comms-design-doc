#!/usr/bin/env python3
#
# Script to template out GitLab issues
#
# Copyright (C) 2021, 2022 Libre Space Foundation <https://libre.space/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import re
from pathlib import Path

import gitlab
import pypandoc
from jinja2 import Environment, FileSystemLoader

# Custom filter method
def re_sub(string, match, replacement):
    rstring = r'{}'.format(match)
    return re.sub(rstring, replacement, string)

def md2tex(text):
    return pypandoc.convert_text(text, format='markdown', to='latex')


# Create argument parser
parser = argparse.ArgumentParser(description="Template out GitLab issues")
parser.add_argument('--project',
                    metavar='PROJECT',
                    type=str,
                    required=True,
                    help='Project ID or URL-encoded path of the project')
parser.add_argument('--label',
                    metavar='LABEL',
                    action='append',
                    type=str,
                    required=True,
                    help='Filter by label (can be specified multiple times)')
parser.add_argument('--output-file',
                    metavar='OUTPUT_FILE',
                    type=str,
                    help='Output file')
parser.add_argument('template',
                    metavar='TEMPLATE_FILE',
                    type=str,
                    help='Template source file')

# Parse arguments
args = parser.parse_args()

# Create GitLab client
gl_client = gitlab.Gitlab.from_config()

# Get project
project = gl_client.projects.get(args.project, lazy=True)

# Get issues with labels
issues = project.issues.list(labels=args.label,
                             iterator=True,
                             state='opened',
                             order_by='created_at',
                             sort='asc')

# Create parsed issues list
items = []
for issue in issues:
    # Parse issue
    desc = re.findall(r'^\*{2}(.*?)\*{2}\n+(.*?)\n*(?=\n\*{2}|\Z)',
                      issue.description, re.MULTILINE | re.DOTALL)
    # Create issue item
    item = {
        'id': issue.iid,
        'title': issue.title,
        'labels': issue.labels,
        'content': dict(desc),
    }

    # Append to items list
    items.append(item)

template_path = Path(args.template)

# Create Jinja2 environment
j2_env = Environment(
    loader=FileSystemLoader('/' if template_path.is_absolute() else ''))
j2_env.filters['md2tex'] = md2tex
j2_env.filters['re_sub'] = re_sub

# Load Jinja2 template
template = j2_env.get_template(str(template_path))

if args.output_file:
    output_path = Path(args.output_file)

    # Write rendered template
    template.stream(issues=items).dump(str(output_path))
else:
    # Render template
    print(template.render(issues=items))
