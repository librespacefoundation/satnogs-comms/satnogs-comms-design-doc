% SatNOGS COMMS System Design Document - User Manual
% Copyright (C) 2020, 2021, 2022, 2023, 2024 Libre Space Foundation
%
% This work is licensed under a
% Creative Commons Attribution-ShareAlike 4.0 International License.
%
% You should have received a copy of the license along with this
% work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.

% Linter settings
% chktex-file -2

\documentclass[english,title,a4paper]{report}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{float}
\usepackage[hidelinks]{hyperref}
\usepackage[toc,acronyms,section,nopostdot,nonumberlist,section=section,numberedsection]{glossaries}
\usepackage[toc]{appendix}
\usepackage{graphicx}
\usepackage{lastpage}
\usepackage{titlesec}
\usepackage{fancyhdr}
\usepackage{lipsum}
\usepackage{listings}
\usepackage{booktabs}
\usepackage[maxfloats=500]{morefloats}
\usepackage{lscape}
\usepackage{makecell}
\usepackage[table]{xcolor}
\usepackage{fixme}
\usepackage{pdfpages}
\usepackage{xcolor}

\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=black,
    filecolor=magenta,
    urlcolor=blue,
    citecolor=black,
}

\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}

\newcommand\TBD[1]{\textcolor{red}{#1}}

\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolour},
    commentstyle=\color{codegreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{codepurple},
    basicstyle=\ttfamily\footnotesize,
    breakatwhitespace=false,
    breaklines=true,
    captionpos=b,
    keepspaces=true,
    numbers=left,
    numbersep=5pt,
    showspaces=false,
    showstringspaces=false,
    showtabs=false,
    tabsize=2
}

\lstset{style=mystyle}

\fancypagestyle{plain}{
  \fancyhf{}
  \fancyhead[L]{\includegraphics[width=1.5cm]{diagrams/LSF_HD_Icon_Color.png}}  % chktex-file 25
  \fancyhead[R]{SatNOGS COMMS\\User Manual}
  \fancyfoot[L]{Version \version}
  \fancyfoot[C]{\thepage{} of~\pageref{LastPage}}
  \fancyfoot[R]{\copyright{} Libre Space Foundation}
  \renewcommand{\headrulewidth}{0.4pt}
  \renewcommand{\footrulewidth}{0.4pt}
}
\pagestyle{plain}
\setcounter{secnumdepth}{3}
\setcounter{tocdepth}{3}

\input{include/version}

\makeglossaries{}

%% Acronyms
\input{glossaries/acronyms}

\title{
  User Manual \\
  \large{SatNOGS COMMS} \\
}
\author{\includegraphics[scale=0.2]{diagrams/lsf-logo.png}}
\date{Version \version}

\titleformat{\chapter}[hang]
{\normalfont\bfseries\Huge}{\thechapter.}{10pt}{}

\begin{document}

\hypersetup{pageanchor=false}
\maketitle
\hypersetup{pageanchor=true}
\tableofcontents
\newpage

\chapter{Changelog}

\begin{tabular}{llp{6.5cm}}
    \toprule
    Version & Date          & Changes                                     \\ \midrule
    1.0     & Dec 18, 2023  & No Changes                                  \\ \midrule
    1.1     & Dec 29, 2023  & No Changes                                  \\ \midrule
    1.2     & Mar 19, 2024  & No Changes                                  \\ \midrule
    1.3     & Apr 30, 2024  & No Changes                                  \\ \midrule
    1.4     & Nov 05, 2024  & No Changes                                  \\ \midrule
    1.5     & Dec 10, 2024  & \textbullet{} Add GSE and connections       \newline 
                              \textbullet{} Update Software Setup chapter \\ \midrule
    1.5.1   & Dec 10, 2024  & Fix static GitLab pipeline                  \\ \midrule
    1.5.2   & Dec 18, 2024  & No changes                                  \\ \midrule
    1.6     &               & \textbullet{} Add FPGA GSE                  \newline 
                              \textbullet{} Add guide for FM preparation  \\ \bottomrule
\end{tabular}

\newpage
\printglossary[type=\acronymtype]

\chapter{Preface}\label{preface}

SatNOGS COMMS is an open hardware and open source software project.
This allows the end user to customize all aspects of the \acrshort{mcu} and \acrshort{fpga} Software to meet their needs.

More information on software can be found in the software repositories under
\url{https://gitlab.com/librespacefoundation/satnogs-comms/}

\chapter{Unpacking and handling}\label{unpacking-and-handling}

\begin{figure}[h!]
\begin{flushleft}
\begin{minipage}{0.4\textwidth}
\begin{center}
    \includegraphics[width=0.5\textwidth]{diagrams/ESD_Protected.png} % chktex-file 25
\end{center}
\end{minipage}
\end{flushleft}
\end{figure}
\vspace{-3cm}
\begin{flushright}
\begin{minipage}{0.6\textwidth}
SatNOGS COMMS is and \acrshort{esd} sensitive device.
Standard \acrshort{esd} handling methods should be followed when handling the \acrshort{pcb}\@.
Always use an \acrshort{esd} mat and \acrshort{esd} wristband.
\end{minipage}
\end{flushright}

\vspace{1cm}

\begin{figure}[h!]
\begin{flushleft}
\begin{minipage}{0.4\textwidth}
\begin{center}
    \includegraphics[width=0.4\textwidth]{diagrams/ISO_7010_M001.png} % chktex-file 25
\end{center}
\end{minipage}
\end{flushleft}
\end{figure}
\vspace{-3.5cm}
\begin{flushright}
\begin{minipage}{0.6\textwidth}
It is advised to wear gloves when handling the system to avoid contamination.
Should cleaning of any parts is required, use \acrshort{esd} safe cleaning methods and non-reactive solvents.
It is recommended to handle the unit in a clean environment using an appropriate laminar flow workbench or an ISO class 8 cleanroom.
\end{minipage}
\end{flushright}

\vspace{1cm}

\begin{figure}[h!]
\begin{flushleft}
\begin{minipage}{0.4\textwidth}
\begin{center}
    \includegraphics[width=0.4\textwidth]{diagrams/ISO_7010_M001.png}
\end{center}
\end{minipage}
\end{flushleft}
\end{figure}
\vspace{-3cm}
\begin{flushright}
\begin{minipage}{0.6\textwidth}
It is recommended to store in a controlled environment with temperatures between \( 15^\circ\mathrm{C} \) and \( 30^\circ\mathrm{C} \), relative humidity of 40\% to 65\%, using appropriate packaging materials such as antistatic bags or moisture barrier bags with desiccants.
\end{minipage}
\end{flushright}

\chapter{Hardware setup}\label{hardware-setup}

\section{RF Connection}\label{rf-connection}

SatNOGS COMMS should always be powered with an antenna or RF load connected to its \acrshort{rf} ports.
Failing to do so may result in catastrophic system failure.

A 50 Ohm antenna or 50 Ohm \acrshort{rf} load capable of sinking 2W of power must be present to each \acrshort{rf} connector at all times.

\section{Power supply}\label{power-supply}

SatNOGS COMMS can operate over a voltage range of 5.5 to 30V DC for version of 0.3.2 and above \@.
This can be provided either through a power supply or directly from the satellite battery.

A power supply should be connected to VIN and GND pins as described in ICD \@.
Adjust the current limit so that a maximum power of 10W is available.

\section{CAN Bus Connection}\label{can-bus-connection}

SatNOGS COMMS uses the CAN BUS transceiver with part number TCAN334G and the pins are exposed throu PC104 interafe as described in ICD \@.
The termination resistor is optional and tunable.

\section{Ground Support Equipment}\label{gse}

\subsection{Part List}
To operate SatNOGS COMMS outside a satellite needs to have addition support equipment:
\begin{itemize}
    \item \underline{Note-1:} \textit{for a FlatSat configuration with other systems you can use PC-104 FlatSat, \url{https://gitlab.com/librespacefoundation/pc104-flatsat}}
\end{itemize}

\begin{figure}[H]
\begin{flushleft}
\begin{minipage}{0.6\textwidth}
\begin{center}
    \includegraphics[width=0.5\textwidth]{diagrams/pc-104-brakout-assy.png}
\end{center}
\end{minipage}
\end{flushleft}
\end{figure}
\vspace{-3.5cm}
\begin{flushright}
\begin{minipage}{0.4\textwidth}
PC-104 Breakout, \url{https://gitlab.com/librespacefoundation/pc104-flatsat/-/releases/v0.1.1#pc104-breakout}
\end{minipage}
\end{flushright}

\vspace{1cm}

\begin{figure}[H]
\begin{flushleft}
\begin{minipage}{0.6\textwidth}
\begin{center}
    \includegraphics[width=0.5\textwidth]{diagrams/can-pinout.jpg}
\end{center}
\end{minipage}
\end{flushleft}
\end{figure}
\vspace{-3cm}
\begin{flushright}
\begin{minipage}{0.4\textwidth}
candleLight FD USB to CAN adapter, \url{https://linux-automation.com/en/products/candlelight-fd.html}
\end{minipage}
\end{flushright}

\vspace{1cm}

\begin{figure}[h!]
\begin{flushleft}
\begin{minipage}{0.6\textwidth}
\begin{center}
    \includegraphics[width=0.5\textwidth]{diagrams/jlink.jpg}
\end{center}
\end{minipage}
\end{flushleft}
\end{figure}
\vspace{-3cm}
\begin{flushright}
\begin{minipage}{0.4\textwidth}
SEGGER J-Link EDUJTAG/SWD Debugger, \url{https://www.adafruit.com/product/1369}, or any other JTAG Interface Connection (20 pin)
\end{minipage}
\end{flushright}

\vspace{1cm}

\begin{figure}[h!]
\begin{flushleft}
\begin{minipage}{0.6\textwidth}
\begin{center}
    \includegraphics[width=0.5\textwidth]{diagrams/JTAG_HS3.png} % chktex-file 25
\end{center}
\end{minipage}
\end{flushleft}
\end{figure}
\vspace{-4cm}
\begin{flushright}
\begin{minipage}{0.4\textwidth}
JTAG-HS3 Programming Cable, \url{https://digilent.com/shop/jtag-hs3-programming-cable/}
\end{minipage}
\end{flushright}

\newpage

\begin{figure}[h!]
\begin{flushleft}
\begin{minipage}{0.6\textwidth}
\begin{center}
    \includegraphics[width=0.4\textwidth]{diagrams/mcu-adapter.png}
\end{center}
\end{minipage}
\end{flushleft}
\end{figure}
\vspace{-4cm}
\begin{flushright}
\begin{minipage}{0.4\textwidth}
MCU Programming Breakout Board, \url{https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-misc#mcu-programming-breakout-board}
\end{minipage}
\end{flushright}

\vspace{1cm}

\begin{figure}[h!]
\begin{flushleft}
\begin{minipage}{0.6\textwidth}
\begin{center}
    \includegraphics[width=0.4\textwidth]{diagrams/fpga-adapter.png}
\end{center}
\end{minipage}
\end{flushleft}
\end{figure}
\vspace{-4cm}
\begin{flushright}
\begin{minipage}{0.4\textwidth}
FPGA Programming Breakout Board, \url{https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-misc#fpga-programming-breakout-board}
\end{minipage}
\end{flushright}

\vspace{1cm}

\begin{figure}[h!]
\begin{flushleft}
\begin{minipage}{0.6\textwidth}
\begin{center}
    \includegraphics[width=0.6\textwidth]{diagrams/pc-104-cable-harness.jpg}
\end{center}
\end{minipage}
\end{flushleft}
\end{figure}
\vspace{-7cm}
\begin{flushright}
\begin{minipage}{0.4\textwidth}
PC-104 Breakout Cable Harness
\begin{itemize}
    \item 2x Molex PicoBlade 15134--1002 (JTAG cables)
    \item DF11--6DS-2C (with crimps DF11--2428SCFA(04)) to 4mm Banana plugs (Power Supply cable, recommended wire: Alpha Wire 3049)
    \item Molex PicoBlade 510210400 (with crimps 50079--8001) to DSUB-9 with backshell (CAN Bus cable, recommended wire: Alpha Wire 3049)
    \item 2x Molex 897616820, male MMCX to female SMA cable, 152.4mm (RF cables)
\end{itemize}
\end{minipage}
\end{flushright}

\newpage

\begin{figure}[H]
\begin{flushleft}
\begin{minipage}{0.6\textwidth}
\begin{center}
    \includegraphics[width=0.6\textwidth]{diagrams/Libre-Cube-Mount.jpg}
\end{center}
\end{minipage}
\end{flushleft}
\end{figure}
\vspace{-7cm}
\begin{flushright}
\begin{minipage}{0.4\textwidth}
Libre Cube Mount, \url{https://gitlab.com/librespacefoundation/pc104-flatsat},
with 4x Essentra CBMFTS335A Standoffs and 4x screws M3 L12 DIN912.
\end{minipage}
\end{flushright}

\vspace{4cm}

\begin{figure}[h!]
\begin{flushleft}
\begin{minipage}{0.6\textwidth}
\begin{center}
    \includegraphics[width=0.5\linewidth]{diagrams/G125-FC22605L0-0150L.jpg}
\end{center}
\end{minipage}
\end{flushleft}
\end{figure}
\vspace{-5cm}
\begin{flushright}
\begin{minipage}{0.4\textwidth}
FPGA debug cable, G125-FC22605L0-0150L with Dupont (2.54mm) crimps and single terminals. 
Each wire should be labeled according to the ICD.
This cable is used from:
\begin{itemize}
    \item UART to USD adapter
    \item RGMII Ethernet PHY
    \item external eMMC reader
\end{itemize}
\end{minipage}
\end{flushright}

\vspace{1cm}

\begin{figure}[h!]
\begin{flushleft}
\begin{minipage}{0.6\textwidth}
\begin{center}
    \includegraphics[width=0.5\linewidth]{diagrams/microsd-sniffer-adapter.jpg}
\end{center}
\end{minipage}
\end{flushleft}
\end{figure}
\vspace{-4cm}
\begin{flushright}
\begin{minipage}{0.4\textwidth}
eMMC external reader, SparkFun microSD Sniffer \url{https://www.sparkfun.com/sparkfun-microsd-sniffer.html}.
In addition to the signals DATA 0, CLK, GND, CMD you need to connect CD (Card Detect) to GND.
In order to have access from external reader you need to set the direction to MCU and you do not need to initialize the eMMC from MCU.
\end{minipage}
\end{flushright}

\newpage

\begin{figure}[h!]
\begin{flushleft}
\begin{minipage}{0.6\textwidth}
\begin{center}
    \includegraphics[width=0.5\linewidth]{diagrams/dp83867ergz-r-evm-angled.png}
\end{center}
\end{minipage}
\end{flushleft}
\end{figure}
\vspace{-3cm}
\begin{flushright}
\begin{minipage}{0.4\textwidth}
DP83867ERGZ RGMII 1000M/100M/10M Ethernet PHY evaluation module \url{https://www.ti.com/tool/DP83867ERGZ-R-EVM}.
Follow the manual of evaluation board to connect the RGMII interface.
The evaluation board needs two power supplies:
\begin{itemize}
    \item 5V on turret pins (5V and GND).
    \item 1.8V is applied on the P2 connector. Pin 3 is VDDIO EXT and pin 7 is VDDA1P8. To provide only 1.8V externally, we need to solder R77, R67, R68 and remove R74.
\end{itemize}
\end{minipage}
\end{flushright}

\vspace{1cm}

\begin{figure}[h!]
\begin{flushleft}
\begin{minipage}{0.6\textwidth}
\begin{center}
    \includegraphics[width=0.5\linewidth]{diagrams/uart2usb.jpg}\end{center}
\end{minipage}
\end{flushleft}
\end{figure}
\vspace{-3cm}
\begin{flushright}
\begin{minipage}{0.4\textwidth}
A UART to USB adapter that supports 3.3V voltage interfaces for FPGA UART interface.
\end{minipage}
\end{flushright}

\vspace{1cm}

\subsection{Connections}

Viewing the breakout board from the top (THIS SIDE UP), the pinout of the Molex PicoBlade CAN connectors is depicted in Figure~\ref{fig:flatsat-can-connector-pinout}.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.8\textwidth]{diagrams/pc104-breakout-can.png}
  \caption{PC/104 Breakout CAN connector pinout}\label{fig:flatsat-can-connector-pinout}
\end{figure}

Viewing the breakout board from the top (THIS SIDE UP), the pinout of the Hirose DF13 power supply connectors is depicted in Figure~\ref{fig:flatsat-power-connector-pinout}.

The power supply input of board is +12V with current limit at 1A.

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{diagrams/pc104-breakout-power.png}
  \caption{PC/104 Breakout power supply connector pinout}\label{fig:flatsat-power-connector-pinout}
\end{figure}

To read and send CAN messages from/to the board we use the candleLight FD USB to CAN adapter.
For convenience during cable manufacturing or on-site cable repair, the connections on the device's D-SUB 9 male connector are provided in \autoref{fig:can-pinout}.

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{diagrams/can-pinout.jpg}
  \caption{D-Sub 9 connector pin numbering for CAN Bus}\label{fig:can-pinout}
\end{figure}

All the set up \autoref{fig:dut}.

\begin{figure}
  \centering
  \includegraphics[width=1.0\textwidth]{diagrams/satnogs-comms-user-manual-setup.jpg}
  \caption{Device under Test}\label{fig:dut}
\end{figure}

\chapter{Software setup}
The SatNOGS COMMS provides a reference firmware~\cite{satnogs-comms-sw-repo} based on the Zephyr-RTOS~\cite{zephyros}.
Most of the requirements and development processes are required by the Zephyr-RTOS itself.
Note that the project is open source, so any user can acquire the code, compile, customize or even report bugs and add new features.
Detailed documentation regarding the compilation, flashing as well as usage of the transceiver can be found at~\cite{satnogs-comms-docs}.

For controlling the transceiver, either through the RF interfaces or one of the wired available (UART, CAN, SPI) the Yamcs-based control software~\cite{satnogs-comms-yamcs-repo} can be used. 
For more details refer to the firmware documentation~\cite{satnogs-comms-docs}.

\begin{appendices}

    \chapter{FPGA SoM installation}\label{app:fpga-installation}
    How to align FPGA SOM with COMMS.
    Tight torque and thread-lock glue apply.
    Log the torques.
    Take photos to confirm alignment.
    
    \chapter{Epoxy glue application (Staking) and Coating}\label{app:epoxy-glue}
    Clean with 99 per cent isopropyl and then with 70-30 isopropyl and demineralize water.
    Use ESD brash and ESD cleaning cloth.
    Inspect with UV and take photos for logs.

    Where to put epoxy?
    The theoretical application points for epoxy glue on the front and back layers of the board are marked with red lines in Figure~\ref{fig:epoxy-glue-front-theoretical} and Figure~\ref{fig:epoxy-glue-back-theoretical} respectively.

    \begin{figure}[H]
      \centering
      \includegraphics[width=\textwidth]{diagrams/epoxy_glue_front.png}
      \caption{Theoretical epoxy glue application points \- front layer}\label{fig:epoxy-glue-front-theoretical}
    \end{figure}

    \begin{figure}[H]
      \centering
      \includegraphics[width=\textwidth]{diagrams/epoxy_glue_back.png}
      \caption{Theoretical epoxy glue application points \- back layer}\label{fig:epoxy-glue-back-theoretical}
    \end{figure}

    Conformal coating is optional.
    For masking use Peelable Coating Mask like TBD.
    Define masking areas.
    Log curing conditions and method.
    Define the coating thickness according to IPC-J-STD-001, space addendum.
    Evaluate the thickness of coating.
    Use test cards to measure it (calibrate spray passes).
    Apply the coating.
    Log the the curing conditions and method.
    Remove peelable mask.
    Inspect with UV and log the photos.
       
    \chapter{EMI Shields installation}\label{app:emi-installation}
    Mention when the external clock reference cables are connected.
    Clean EMI shiels with 99 per cent isopropyl.
    Inspect with UV and log the photos.
    How to put EMI gasket.
    Inspect and log the photos.
    Tight torque and thread-lock glue apply.
    Inspect and log the photos.
    EMI shields assembly sequence.
    Inspect and log the photos.

\end{appendices}

\renewcommand\bibname{References}
\bibliographystyle{unsrt}
\bibliography{references}
\end{document}
