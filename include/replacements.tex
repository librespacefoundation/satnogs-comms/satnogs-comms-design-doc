%% SatNOGS COMMS System Design Document - Requirements
%% Copyright (C) 2023 Libre Space Foundation
%%
%% This work is licensed under a
%% Creative Commons Attribution-ShareAlike 4.0 International License.
%%
%% You should have received a copy of the license along with this
%% work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.

% Linter settings
% chktex-file 35

% Switch implementation
\newcommand{\ifequals}[3]{\ifthenelse{\equal{#1}{#2}}{#3}{}}
\newcommand{\case}[2]{#1 #2} % Dummy, so \renewcommand has something to overwrite...
\newenvironment{switch}[1]{\renewcommand{\case}{\ifequals{#1}}}{}

% Equipment Switch
\newcommand{\equip}[1]{%
    \begin{switch}{#1}%
        \case{AdapterPlate}{Adapter plate to mount DUT on shaker table in X, Y and Z configuration}%
        \case{AntennaDeployLoopConnector}{Antenna Deploy Loop Connector}%
        \case{Attenuator20dB}{Attenuator, 20dB, 50 Ohm}%
        \case{Attenuators}{Attenuators}%
        \case{BenchPowerSupply}{Bench Power Supply}%
        \case{SpikeGenerator}{ Spike generator with the following characteristics: \begin{itemize}
        \item Pulse width of 10us and 0.15us
        \item Pulse repetition rate capability up to 10 pulses per second
        \item Voltage output as required, positive then negative
        \item Output control
        \item Adequate transformer current capability commensurate with line being tested
        \item Output impedance 5 Ohm or less for 0.15 us and 1 Ohm or less for 10 us transient
        \item External synchronization and triggering capability
        \end{itemize}}%
        \case{CANCable}{\acrshort{can} cable (length 800mm)}%
        \case{CANSoftware}{\acrshort{can} Bus communication software}%
        \case{CANTransceiver}{\acrshort{can} Bus transceiver}%
        \case{Cap10u}{Capacitor, 10uF}%
        \case{Caliper}{Caliper, with range 0--150mm and accuracy ±0.02mm}%
        \case{CalibrationFixture}{Calibration fixture \: coaxial transmission line with 50 ohm characteristic impedance, coaxial connections on both ends, and space for an injection probe around the center conductor}%
        \case{CouplingTransformer}{Coupling Transformer}%
        \case{CoaxialLoads}{Coaxial Loads, 50 Ohm}%
        \case{CurrentInjectionProbes}{Current injection probes. Maximum insertion loss according to Figure CS114--2 from MIL-STD-461F}%
        \case{CurrentProbes}{Current Probes}%
        \case{DCLoad}{\acrshort{dc} Load}%
        \case{DMMWithCurrentMeas}{\acrshort{dmm} with current measurement capability}%
        \case{DataRecordingDevice}{Data recording device}%
        \case{DebugInterface}{Debug interface}%
        \case{DirectionalCouplers}{Directional Couplers}%
        \case{DualBenchPowerSupply}{Dual channel Bench Power Supply}%
        \case{EFieldSensors}{Electric field sensors or receive antennas}%
        \case{Flatsat}{Flatsat configuration}%
        \case{Grafana}{Grafana Dashboard service configured to use Influx DB database}%
        \case{InfluxDB}{Influx DB database}%
        \case{IsolationTransformer}{Isolation Transformer}%
        \case{LISN}{\acrshort{lisn} s. Inductance should be 50uH, but 5uH is also allowed if its impedance matches the impedance profile specified in MIL-STD-461F Figure A-3 within an 20\% tolerance zone}%
        \case{MMCXToSMA}{\acrshort{mmcx} to \acrshort{sma} cable}%
        \case{MeasurementReceiver}{Measurement receiver (spectrum analyzer)}%
        \case{Oscilloscope}{Oscilloscope with at least 50 MHz bandwidth}%
        \case{PCWithIBOM}{A \acrshort{pc} that has the interactive \acrshort{bom} of testing \acrshort{pcb}}%
        \case{PCWithIDE}{\acrshort{pc} with installed development environment and tools}%
        \case{PCWithSchematic}{A \acrshort{pc} that has the release schematic of testing \acrshort{pcb}}%
        \case{PCWithSourceCode}{A \acrshort{pc} that has the release source code of firmware that is flashed into testing \acrshort{pcb}}%
        \case{PowerAmplifier}{Power amplifier, 20dBm, P1dB}%
        \case{PowerCable}{Power Cable}%
        \case{RFSplitter}{\acrshort{rf} splitter}%
        \case{RFSwitch}{An \acrshort{rf} Switch with 3 \acrshort{sma} ports}%
        \case{RealTimeSpectrumAnalyzer}{Real time spectrum analyzer}%
        \case{ReferenceGS}{Reference \acrshort{gs} setup}%
        \case{Resistor100k}{100k resistor}%
        \case{Resistor0R5}{0.5 Ohm resistor}%
        \case{Resistor5}{5 Ohm resistor power metal film resistor with inductance lower than 100 nH and peak power capability}%
        \case{Resistor1M}{1M resistor}%
        \case{SDR}{\acrshort{sdr} device}%
        \case{SMAToMMCX}{\acrshort{sma} to \acrshort{mmcx} Cable}%
        \case{SMAToN}{\acrshort{sma} to N cable}%
        \case{SMAToSMA}{\acrshort{sma} to \acrshort{sma} cable}%
        \case{SPICable}{\acrshort{spi} cable (length 800mm)}%
        \case{RGMIItoEthernet}{An adapter to convert RGMII to ethernet 10, 100}%
        \case{SPISoftware}{\acrshort{spi} communication software}%
        \case{SatNOGSDBWithInflux}{\acrshort{satnogs} \acrshort{db} service configured to use Influx DB database}%
        \case{SatNOGSNetworkWithSatNOGSDB}{\acrshort{satnogs} Network service configured to use \acrshort{satnogs} \acrshort{db} instance}%
        \case{Scale}{Scale with weighting capacity range: 10Kg and resolution: 0.1g}%
        \case{ShakerTable}{Shaker table}%
        \case{SignalGenerator}{Signal generator}%
        \case{SpectrumAnalyzer}{Spectrum Analyzer}%
        \case{TCCSoftware}{\acrshort{tcc} Software}%
        \case{TVAC}{\acrshort{tvac}}%
        \case{TXSDR}{\acrshort{tx} capable \acrshort{sdr} device}%
        \case{TempControlContainer}{Temperature controlled container}%
        \case{TestPCWithDocOfCOMMS}{Test \acrshort{pc} with access to the documentation of the \acrshort{comms}}%
        \case{TestPCWithDocofGS}{Test \acrshort{pc} with access to the documentation of the \acrshort{gs}}%
        \case{TestPC}{Test \acrshort{pc}}%
        \case{ThermalIsolation}{Thermal isolating suspender or base (depends on the type of the \acrshort{tvac} chamber)}%
        \case{TimeSeriesDBServer}{Time series database server}%
        \case{USBToCAN}{\acrshort{usb} to \acrshort{can} (and \acrshort{canfd}) Device}%
        \case{USBToSPI}{\acrshort{usb} to \acrshort{spi} Device}%
        \case{USRP}{\acrshort{usrp} B210 \acrshort{sdr} device}%
        \case{VariousAntennas}{Biconical antenna for the 30 MHz-400 MHz range, \acrshort{pcb} Log-Periodic antenna for the 400 MHz-1000 MHz range, \acrshort{pcb} Log-Periodic antenna for the 1000 MHz-3.2 GHz range}%
        \case{Visualization}{Telemetry visualization web service}%
        \case{XilinxDebugProbe}{Xilinx Debug probe}%
        \case{grleo}{gr-leo installation}%
    \end{switch}%
}

% Set Values Switch
\newcommand{\setval}[1]{%
    \begin{switch}{#1}%
        \case{dcdc-3v3-dc-load-test-current}{450mA}%
        \case{dcdc-3v3-dc-load-test-voltage-limit}{3.2V}%
        \case{dcdc-3v3-testpoint}{JP501}%
        \case{dcdc-5v-fpga-dc-load-test-current}{450mA}%
        \case{dcdc-5v-fpga-load-test-voltage-limit}{4.95V}%
        \case{dcdc-5v-fpga-testpoint}{TP505}%
        \case{dcdc-5v-rf-dc-load-test-current}{2.4A}%
        \case{dcdc-5v-rf-load-test-voltage-limit}{4.9V}%
        \case{dcdc-5v-rf-testpoint}{TP506}%
        \case{s-band-rx-power-consumption-max}{6.3W}%
        \case{s-band-tx-power-consumption-max}{8.3W}%
        \case{temperature-sensor-deviation-threshold}{1°C}%
        \case{uhf-rx-power-consumption-max}{5.5W}%
        \case{uhf-tx-power-consumption-max}{8W}%
    \end{switch}%
}