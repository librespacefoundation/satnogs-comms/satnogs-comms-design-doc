 % SatNOGS COMMS System Design Document - TVAC and vibration test
% Copyright (C) 2020, 2021, 2022, 2023, 2024 Libre Space Foundation
%
% This work is licensed under a
% Creative Commons Attribution-ShareAlike 4.0 International License.
%
% You should have received a copy of the license along with this
% work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.

% Linter settings
% chktex-file -2
% chktex-file 35

\documentclass[english,title,a4paper]{report}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{float}
\usepackage[hidelinks]{hyperref}
\usepackage[toc,acronyms,section,nopostdot,nonumberlist,section=section,numberedsection]{glossaries}
\usepackage[toc,page]{appendix}
\usepackage{graphicx}
\usepackage{titlesec}
\usepackage{amssymb}
\usepackage{textcomp,gensymb}
\usepackage{booktabs}
\usepackage{pifont}
\usepackage{verbatim}
\usepackage{rotating}
\usepackage{makecell}
\usepackage{lipsum}
\usepackage{longtable}
\usepackage{rotating}
\usepackage{indentfirst}
\usepackage{multirow}
\usepackage{array}
\usepackage{fancyhdr}
\usepackage{lastpage}
\usepackage{xcolor, soul}
\sethlcolor{yellow}
\usepackage[yyyymmdd]{datetime}
\renewcommand{\dateseparator}{-}
\newcolumntype{C}[1]{>{\centering\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}

\newcommand{\cmark}{\ding{51}}%
\newcommand{\xmark}{\ding{55}}%
\newcommand\TBD[1]{\textcolor{red}{#1}}

\fancypagestyle{plain}{
  \fancyhf{}
  \fancyhead[L]{\includegraphics[width=1.5cm]{diagrams/LSF_HD_Icon_Color.png}} %chktex-file 25
  \fancyhead[R]{SatNOGS COMMS\\Environmental Test \:: TVAC \& Vibration}
  \fancyfoot[L]{Version \version}
  \fancyfoot[C]{\thepage{} of~\pageref{LastPage}}
  \fancyfoot[R]{\copyright{} Libre Space Foundation}
  \renewcommand{\headrulewidth}{0.4pt}
  \renewcommand{\footrulewidth}{0.4pt}
}
\pagestyle{plain}
\setcounter{secnumdepth}{3}
\setcounter{tocdepth}{3}

\input{include/version}
\input{include/test-cases-urls}
\input{include/requirements-urls}
\input{include/replacements.tex}

% \makeglossaries{}

%% Acronyms
\input{glossaries/acronyms}

\title{Environmental Test \:: TVAC \& Vibration\\\large{SatNOGS COMMS}}
\author{\includegraphics[scale=0.2]{diagrams/lsf-logo.png}}
\date{Version \version}

\titleformat{\chapter}[hang]
{\normalfont\bfseries\Huge}{\thechapter.}{10pt}{}

\begin{document}

\hypersetup{pageanchor=false}
\maketitle
\hypersetup{pageanchor=true}
\tableofcontents

\chapter{Changelog}

\begin{tabular}{llp{6.5cm}}
    \toprule
    Version & Date          & Changes                                     \\ \midrule
    1.0     & Dec 18, 2023  & Initial release                             \\ \midrule
    1.1     & Dec 29, 2023  & No Changes                                  \\ \midrule
    1.2     & Mar 19, 2024  & No Changes                                  \\ \midrule
    1.3     & Apr 30, 2024  & No Changes                                  \\ \midrule
    1.4     & Nov 05, 2024  & No Changes                                  \\ \midrule
    1.5     & Dec 10, 2024  & No Changes                                  \\ \midrule
    1.5.1   & Dec 10, 2024  & Fix static GitLab pipeline                  \\ \midrule
    1.5.2   & Dec 18, 2024  & No changes                                  \\ \bottomrule
\end{tabular}

\newpage
\printglossary[type=\acronymtype]

\chapter{Introduction}

This document outlines the required equipment, setup and test procedures for the vibration and \acrshort{tvac} tests of the SatNOGS COMMS board.
The present version of the documents refers to SatNOGS COMMS v0.2, while interfaces and specific equipment refer to the vibration table and \acrshort{tvac} chamber of NanoSat Lab \-- UPC in Barcelona.
The purpose of the document is to serve as a means of preparation before the test, guide during it as well as a means of result presentation.

\chapter{Packing List}

Listed below is the equipment one must bring to the test site.
User has the option of pre-assembling some parts to ease the on-site preparations, nevertheless the list is provided in a fragmented fashion.
Minimum quantities are listed and the user can pack more according to their judgement.

\section{Boards and Devices}

\begin{itemize}
    \item SatNOGS COMMS board v0.2
    \item FPGA SoM
    \item EMI Shield compatible with SatNOGS COMMS v0.2
    \item PC/104 FlatSat board (any version)
    \item Lattice HW-USBN-2B programmer with cables for SatNOGS-COMMS and PC
    \item Segger-J-link debuger with cables for SatNOGS-COMMS and PC
    \item JTAG-HS3 programmer with cables for SatNOGS-COMMS and PC
    \item 50 Ohm male SMA terminators
    \item RF Attenuators
    \item DC power supply capable of at least 1A$@$12V and with current limit function
    \item Vernier caliper with at least 150mm measurement capability
    \item Scale with 0.1g accuracy
\end{itemize}

\section{Mounting and Connection hardware}

\begin{itemize}
    \item (4$\times$) SatNOGS COMMS adapter plate mounting bolts (M3$\times$6 DIN912)
    \item (4$\times$) FPGA SoM mounting bolts (M2$\times$4 DIN7985)
    \item (4$\times$) FPGA SoM spacers (M2$\times$3 male to female, solder male to board)
    \item (11$\times$) EMI Shield mounting bolts (M2$\times$8 DIN912)
    \item (2$\times$) SatNOGS COMMS to PC/104 FlatSat mounting bolts (M3$\times$5 DIN912)
    \item (2$\times$) SatNOGS COMMS to PC/104 FlatSat mounting nuts (M3)
    \item (2$\times$) SatNOGS COMMS to PC/104 FlatSat mounting spacers (M3$\times$11 male to female)
    \item (3$\times$) Shunt jumpers for the PC/104 FlatSat (2.54mm pitch)
    \item Allen key for M3 DIN912
    \item Allen key for M2 DIN912
    \item Philips screw driver PH0
    \item Semi-permanent (loctite 243-blue) thread-locker
    \item Torque wrench with proper bits for M3 DIN912, M2 DIN912 and PH0\@.
    Min measurable torque must be 0.5 Nm or less and max measurable torque must be 15 Nm or higher.
    \item Crimper for U-terminals
\end{itemize}

\section{Cables outside TVAC chamber}

\begin{itemize}
    \item D-Sub 9 connector for debugging (continuity checks)
    \item (2x) Male SMA to male SMA cable
    \item (1x) Cable with a female D-Sub 9 connector on the one end and the following on the other
    \begin{itemize}
        \item 4-pin JST-GH (CAN-H, CAN-L, GND, not used)\@.
        Alternatively, use dupont 2.54mm and a JST-GH to dupont cable.
        \item 2 Banana jacks (12V, GND)
    \end{itemize}
\end{itemize}

\section{Cables inside TVAC chamber}

\begin{itemize}
    \item (2$\times$) Male MMCX to male SMA cable. 
    In case it is not available, it can easily be composed of
    \begin{itemize}
        \item (2$\times$) Male MMCX to female SMA cable
        \item (2$\times$) Male SMA to male SMA cable
    \end{itemize}
    \item (1$\times$) Cable with a female D-Sub 9 connector on the one end and the following on the other
    \begin{itemize}
        \item 4-pin Molex Picoblade (CAN-H, CAN-L, GND, not used)
        \item 2 Banana jacks (12V, GND)\@.
        Alternatively, use U-terminals.
    \end{itemize}
\end{itemize}

\section{Pinout and equipment information}

\subsection{RF equipment information}

Some more specific information about the RF cables, terminators and attenuators used are provided in Table~\ref{tab:TVAC-cable-losses}.

\begin{table}[H]
\centering
\begin{tabular}{|c|c|c|}
\hline
Component                  & Model                         & Comment                                                                                          \\ \hline
Attenuators                & Mini-circuit $BW-SxW2+$       & \begin{tabular}[c]{@{}c@{}}2$\times$40, 20, 10, 6, 3 dB\\ Rated at 1W\end{tabular}               \\ \hline
Terminators 50 Ohm         & Mini-circuit $MCL ANNE-50+$ & Rated at 1W                                                                                      \\ \hline
male SMA to male SMA cable & Mini-circuit $FL086--248M$       & \begin{tabular}[c]{@{}c@{}}Measured -0.3dB at 400 MHz\\ Measured -1.2dB at 2400 MHz\end{tabular} \\ \hline
male MMCX to female SMA    & unknown                       & \begin{tabular}[c]{@{}c@{}}Measured -0.5dB at 400 MHz\\ Measured -2dB at 2400 MHz\end{tabular}   \\ \hline
\end{tabular}
\caption{RF hardware information}\label{tab:TVAC-cable-losses}
\end{table}



\subsection{PC/104 FlatSat CAN connector pinout}

Viewing the FlatSat board from the top (logos visible), the pinout of the Molex PicoBlade CAN connectors is depicted in Figure~\ref{fig:flatsat-can-connector-pinout}.

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{diagrams/flatsat-can-connector-pinout.png}
  \caption{PC/104 FlatSat CAN connector pinout}\label{fig:flatsat-can-connector-pinout}
\end{figure}

\subsection{Zubax Babel CAN connector pinout}

To read and send CAN messages from/to the board we use the Zubax Babel USB to CAN adapter.
For convenience during cable manufacturing or on-site cable repair, the connections on the device's JST-GH connector are provided in Figure~\ref{fig:zubax-babel-pinout}, which can be found in its datasheet.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.7\textwidth]{diagrams/zubax-babel-pinout.png}
  \caption{Zubax Babel pinout}\label{fig:zubax-babel-pinout}
\end{figure}

\section{D-Sub 9 pinout}

Note that for the NanoSat Lab TVAC chamber the internal and external D-Sub 9 connectors have a mirrored pinout.
This means that Pin-1 of the external connector is connected with Pin-5 of the internal.
For the \textbf{external} D-Sub 9 connector we chose to use the \href{https://en.wikipedia.org/wiki/CANopen}{CANopen} pinout.
Pin assignments and suggested cable colors can be found in Table~\ref{tab:D-Sub-9-pinout} (cable colors not mentioned in CANopen).
The pinout for the \textbf{internal} connector will be a mirror of the one in the table.
The pin numbering on a D-Sub 9 connector is depicted in Figure~\ref{fig:D-Sub-9-pin-numbering}.

\begin{table}[H]
\centering
\begin{tabular}{|c|c|c|}
\hline
Pin & Function                                                                                          & Cable color \\ \hline
1   & reserved                                                                                          & none        \\ \hline
2   & CAN-L                                                                                             & blue        \\ \hline
3   & \begin{tabular}[c]{@{}c@{}}signal GND\\ (connected with power GND \\ on the FlatSat)\end{tabular} & grey        \\ \hline
4   & reserved                                                                                          & none        \\ \hline
5   & optional shield                                                                                   & none        \\ \hline
6   & optional power GND                                                                                & black       \\ \hline
7   & CAN-H                                                                                             & yellow      \\ \hline
8   & reserved                                                                                          & none        \\ \hline
9   & optional external power supply                                                                    & red         \\ \hline
\end{tabular}\label{tab:D-Sub-9-pinout}
\caption{D-Sub 9 pinout}
\end{table}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.3\textwidth]{diagrams/D-Sub-9-pin-numbering.png}
  \caption{D-Sub 9 connector pin numbering}\label{fig:D-Sub-9-pin-numbering}
\end{figure}

\chapter{Software and Logs}

To ensure firmware traceability, the two following actions must be performed.
\begin{enumerate}
    \item Record by commit or release the software that is run in MCU of SatNOGS COMMS
    \item Record by commit or release the software that is run in FPGA of SatNOGS COMMS
    \item Log all the commands that are send though CAN to SatNOGS COMMS and the results with a UTC timestamp
    \item Log all RF inputs/outputs of the SatNOGS COMMS in I/Q format
\end{enumerate}

\begin{table}[H]
\begin{tabular}{|c|l|}
\hline
\textbf{Test Date}               & \textit{\textbf{\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;}} \\ \hline
\textbf{Testers}                 & \begin{tabular}[c]{@{}l@{}}\\\\ \\\\ \\\end{tabular}                                                                                                                             \\ \hline
\textbf{MCU SW Commit Hash}      &                                                                                                                                                                                  \\ \hline
\textbf{FPGA Config Commit Hash} &                                                                                                                                                                                  \\ \hline
\end{tabular}
\end{table}

\chapter{Vibration test}

\section{DUT assembly for the vibration test}

Assembly procedures for the \acrshort{dut} are provided below in order of precedence.
Is it strongly suggested that all of the assembly procedures listed below are performed in the lab, not on the test site.

\begin{enumerate}
    \item Apply epoxy glue on the bulky components of the board.
    More information on this step can be found under Appendix~\ref{app:epoxy-glue}
    \item Assemble the \acrshort{fpga} SoM on SatNOGS COMMS
    \begin{enumerate}
        \item Use M2$\times$4 DIN7985 bolts.
        \item Apply semi-permanent (blue) thread-locker.
        \item Tighten the bolts with 0.5 Nm cross pattern.
    \end{enumerate}
    \item Assemble the \acrshort{emi} shield on SatNOGS COMMS
    \begin{enumerate}
        \item Use M2$\times$8 DIN912 bolts.
        \item Apply semi-permanent (loctite 243-blue) thread-locker
        \item Tighten the bolts with 0.5 Nm cross pattern
    \end{enumerate}
\end{enumerate}

\section{Connection to the vibration table}
\begin{enumerate}
    \item Connect the adapter plate to the vibration table
    \begin{enumerate}
        \item Use (8$\times$) M8$\times$25 DIN912 bolts
        \item Tighten the bolts with 15 Nm cross pattern
    \end{enumerate}
    \item Connect SatNOGS COMMS to the adapter plate.
    \begin{enumerate}
        \item Use (4$\times$) M3$\times$6 DIN912 bolts
        \item Apply semi-permanent (loctite 243-blue) thread-locker
        \item Tighten the bolts with 1.2 Nm cross pattern
    \end{enumerate}
\end{enumerate}

\section{Vibration Test Procedure}

Follow Appendix~\ref{app:vibration-tests}.
Mark the axes as defined in the LibreCube board specification, which are depicted in Figure~\ref{fig:librecube-axes}.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.7\textwidth]{diagrams/librecube-axes.png}
  \caption{Axes definition according to the LibreCube specification}\label{fig:librecube-axes}
\end{figure}

\chapter{TVAC test}

\section{Connection to the TVAC chamber}

This section describes the necessary connections that must be made to the device in order to perform the needed tests inside the TVAC chamber.

\begin{enumerate}
    \item Connect SatNOGS COMMS (assembled with the EMI shield) on the PC/104 FlatSat\@.
    It must be ensured that the bolts are tight enough, for which turning the bolt half a turn after it has been fully bolted is enough.
    \item Connect the Molex PicoBlade connector to the PC/104 FlatSat
    \item Connect the banana  (or U-) terminals to the PC/104 FlatSat
    \item Ensure the required shunt jumpers (CAN-H, CAN-L, individual board power) are connected
    \item Connect the internal D-Sub 9 connector
    \item Connect the external D-Sub 9 connector
    \item Connect  the JST-GH connector to the CAN-USB device, confirming the connection via the wire color coding, table~\ref{tab:D-Sub-9-pinout}, for CAN-H and CAN-L
    \item Connect the power terminals to the power supply, tuned at 12V and 1A current limit
    
\end{enumerate}

\section{Tests before and after TVAC}

In order to determine mass loss due to outgassing, as well as dimension changes due to the thermal cycling, one must perform the dimensions and mass measurements (Appendix~\ref{app:dimension-measurement} and Appendix~\ref{app:mass-measurement} respectively) before and after the thermal cycling.
To consider the mass loss test successful, the difference in mass before and after the thermal cycling should be less than 1\%.

Moreover, if it has not been performed earlier, a functional test as per Section~\ref{sec:functional-test}. 

\newpage

\section{Tests in the TVAC environment}
Besides mass loss and out-gassing tests, we need to perform some performance/function related tests inside the \acrshort{tvac} environment.
Those must be repeated for the high and low temperature plateau.
% \TBD{i think that the procedure of mass loss is missing from the appendix, also it need to measure the dimensions of DUT could you please add in appendix the test case \#10 and \#9} 

% \TBD{Satellites should be weighted before and after the procedure and the difference in mass should be less than 1\%  and test case \#9 for the test to be succeeded.}
% \TBD{Satellites should be measured before and after the procedure and the expected results defined in test case \#10 for the test to be succeeded.}
% \TBD{Consider to follow the below process in plateau instead of the Functional test as described in test case.}


\begin{enumerate}
    \item Establish communication with the board through CAN
    \item Command the board to open 5V\_FPGA and 5V\_RF
    \item Command the board to open FPGA
    \item Attempt FPGA-MCU communication via SPI and get the confirmation via CAN
    \item Get temperatures (board, UHF-PA, Sband-PA) through CAN
    \item Get status and measurements (voltage and load) from power rails
    \item Repeat the following for the 2 radios
    \begin{enumerate}
        \item In the case of S-Band radio, get the lock of RF-mixer
        \item Begin TX of a CW (or FSK-50kHz modulation) using the baseband core
        \begin{itemize}
            \item Register $AT86RF215-PACUR-NO-RED = 25$
            \item Frequency should be 432MHz for UHF, 2240MHz for S-Band
        \end{itemize}
        \item Measure the center frequency of the transmitted signal
        \item Repeat the above test for each radio for total time 1800sec (30minutes) with interval of 60sec (1min), 30 packets in total\@.
        For example start with S-Band then with UHF for hot plateau and then in the next hot plateau start with UHF then with S-Band.
    \end{enumerate}
\end{enumerate}

\chapter{Functional test definition}\label{sec:functional-test}

A functional test is performed before and after the vibration excitation of the board.
The test performed before the excitation confirms the board's flawless operational state (or identifies malfunctions that existed before the test) and, thus, serves as a control.
The test performed after the excitation aims to detect any malfunctions caused by it.
In this sense, a functional test is constituted by the following procedures. 

\begin{enumerate}
    \item Power up the board by connecting 12V power with 1A current limit to the PC/104 FlatSat banana (or U-) terminals
    \item Establish communication with the board though CAN 
    \item Command the board to open 5V\_FPGA and 5V\_RF power rails
    \item Command the board to open FPGA
    \item Command the board to communicate with FPGA via SPI
    \item Ask for (and receive via CAN) the following information
    \begin{enumerate}
        \item Voltages on the power rails
        \item Loads (current) on the power rails
        \item Check for any power failure flags from the supervisors
        \item Temperatures (board, UHF-PA, Sband-PA)
        \item Success of SPI communication with the FPGA
    \end{enumerate}
    \item Repeat the following for both bands
    \begin{enumerate}
        \item Begin TX with FSK-50kHz modulation using the baseband core
        \begin{itemize}
            \item Register $AT86RF215-PACUR-NO-RED = 25$
            \item Frequency should be 432MHz for UHF, 2240MHz for S-Band
        \end{itemize}
        \item In the case of S-Band radio get the lock of RF-mixer
        \item Measure TX power
        \item Measure TX spurious emissions
        \item Measure TX spectrum mask
        \item Confirm integrity of transmitted data
        % ----- RX IS CURRENTLY NOT AT A STABLE STATE SO WE ARE NOT TESTING IT -----
%         \item Stop TX, configure the board for RX\@.
% Feed FSK-modulated signal with max acceptable data rate to the board\@.
% Use the full AGC mechanism, not the bypass\@.
%         \item Confirm the integrity of the received data via a response through CAN \TBD{,RX isn't in stable state, no test}
        \item In the case of Sband radio, repeat the TX steps and measurements in the maximum symbol rate of the baseband core (FSK-400kHz)
    \end{enumerate}
\end{enumerate}

\begin{appendices}

    \chapter{Epoxy glue application}\label{app:epoxy-glue}
    The theoretical application points for epoxy glue on the front and back layers of the board are marked with red lines in Figure~\ref{fig:epoxy-glue-front-theoretical} and Figure~\ref{fig:epoxy-glue-back-theoretical} respectively.
    
    \begin{figure}[H]
      \centering
      \includegraphics[width=\textwidth]{diagrams/epoxy_glue_front.png}
      \caption{Theoretical epoxy glue application points \- front layer}\label{fig:epoxy-glue-front-theoretical}
    \end{figure}

    \begin{figure}[H]
      \centering
      \includegraphics[width=\textwidth]{diagrams/epoxy_glue_back.png}
      \caption{Theoretical epoxy glue application points \- back layer}\label{fig:epoxy-glue-back-theoretical}
    \end{figure}

    \chapter{Test Procedures}\label{app:vibration-tests}

    In order to reduce the number of required documents needed during the on-site procedures, the Environmental test section from the Test Readiness Document is also included here.

    \IfFileExists{include/test-cases}{\input{include/test-cases}}{} % chktex 27

    \ifdefined\Environmental{}
    \Environmental{}
    \fi
    \clearpage

    \chapter{Dimension measurement}\label{app:dimension-measurement}

    The test case related to the measurement of the board's dimensions is included in this Appendix.

    \textbf{Description}

    Measure the physical dimensions of the COMMS and verify it does not
    exceed 96mm x 96mm x 35mm and that mounting holes follow
    \href{https://web.archive.org/web/20220610143119/https://librecube.gitlab.io/standards/board_specification/}{LibreCube
    standard}.
    
    \textbf{Requirements}
    
    https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-org/-/issues/8
    
    \textbf{Equipment}
    
    \begin{itemize}
    \item Caliper
    \end{itemize}
    
    \textbf{Sequence}
    
    \begin{enumerate}
    \def\labelenumi{\arabic{enumi}.}
    \item Use caliper to measure the length and width
    \item Use caliper to measure the height between the highest component on bottom side and the highest component on top side of COMMS
    \item Use caliper to measure the position of the four mounting holes, starting from origin and move counter clock wise as described in \href{https://web.archive.org/web/20220610143119/https://librecube.gitlab.io/standards/board_specification/}{LibreCube
      standard}
    \item Use caliper to measure the diameter of four mounting holes
    \end{enumerate}
    
    \textbf{Expected Result}
    
    \begin{enumerate}
    \def\labelenumi{\arabic{enumi}.}
    \item The length is 90.17 mm, width is 95.89 mm
    % \item The height is 13.59mm
    \item The position of four holes are at (5.08, 5.08) mm, (85.09, 5.08) mm, (82.55, 90.81) mm and (8.89, 90.81) mm from origin (0,0) mm
    \item The diameter of four holes is 3.18 mm
    \end{enumerate}

    \chapter{Mass measurement}\label{app:mass-measurement}

    The test case related to the measurement of the board's mass is included in this Appendix.

    \textbf{Description}

    Measure the mass of the COMMS and verify it does not exceed 250 g.
    
    \textbf{Requirements}
    
    https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-org/-/issues/63
    
    \textbf{Equipment}
    
    \begin{itemize}
    \item Scale
    \end{itemize}
    
    \textbf{Sequence}
    
    \begin{enumerate}
    \def\labelenumi{\arabic{enumi}.}
    \item Use the scale to measure the mass of COMMS
    \end{enumerate}
    
    \textbf{Expected Result}
    
    The mass must be less than 250g.


\end{appendices}

\end{document}
