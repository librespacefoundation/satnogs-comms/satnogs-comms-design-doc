% SatNOGS COMMS System Requirements Specification
% Copyright (C) 2021, 2022, 2023, 2024 Libre Space Foundation
%
% This work is licensed under a
% Creative Commons Attribution-ShareAlike 4.0 International License.
%
% You should have received a copy of the license along with this
% work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.

% Linter settings
% chktex-file -2

\documentclass[english,title,a4paper]{report}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{float}
\usepackage[hidelinks]{hyperref}
\usepackage[toc,acronyms,section,nopostdot,nonumberlist,section=section,numberedsection]{glossaries}
\usepackage[toc,page]{appendix}
\usepackage{graphicx}
\usepackage{titlesec}
\usepackage{booktabs}
\usepackage[hashEnumerators,pipeTables,tableCaptions]{markdown}
\usepackage[maxfloats=500]{morefloats}
\usepackage{fancyhdr}
\usepackage{lastpage}

\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=black,
    filecolor=magenta,
    urlcolor=blue,
    citecolor=black,
}

\fancypagestyle{plain}{
  \fancyhf{}
  \fancyhead[L]{\includegraphics[width=1.5cm]{diagrams/LSF_HD_Icon_Color.png}} %chktex-file 25
  \fancyhead[R]{SatNOGS COMMS\\System Requirements Specification}
  \fancyfoot[L]{Version \version}
  \fancyfoot[C]{\thepage{} of~\pageref{LastPage}}
  \fancyfoot[R]{\copyright{} Libre Space Foundation}
  \renewcommand{\headrulewidth}{0.4pt}
  \renewcommand{\footrulewidth}{0.4pt}
}
\pagestyle{plain}
\setcounter{secnumdepth}{3}
\setcounter{tocdepth}{3}

\maxdeadcycles=1000
\markdownSetup{
  renderers = {
    ampersand = \&,
    backslash = \textbackslash,
    circumflex = \textasciicircum,
    dollarSign = \$,
    hash = \#,
    leftBrace = \{,
    percentSign = \%,
    pipe = \textpipe,
    rightBrace = \},
    tilde = \textasciitilde,
    underscore = \_,
  },
}

\input{include/version}

\makeglossaries{}

%% Acronyms
\input{glossaries/acronyms}

\title{System Requirements Specification \\\large{SatNOGS COMMS}}
\author{\includegraphics[scale=0.2]{diagrams/lsf-logo.png}}
\date{Version \version}

\titleformat{\chapter}[hang]
{\normalfont\bfseries\Huge}{\thechapter.}{10pt}{}

\begin{document}

\hypersetup{pageanchor=false}
\maketitle
\hypersetup{pageanchor=true}
\tableofcontents

\chapter{Changelog}

\begin{tabular}{llp{6.5cm}}
    \toprule
    Version & Date          & Changes                                     \\ \midrule
    1.0     & Dec 18, 2023  & No Changes                                  \\ \midrule
    1.1     & Dec 29, 2023  & No Changes                                  \\ \midrule
    1.2     & Mar 19, 2024  & No Changes                                  \\ \midrule
    1.3     & Apr 30, 2024  & No Changes                                  \\ \midrule
    1.4     & Nov 05, 2024  & No Changes                                  \\ \midrule
    1.5     & Dec 10, 2024  & Add use cases and actors                    \\ \midrule
    1.5.1   & Dec 10, 2024  & Fix static GitLab pipeline                  \\ \midrule
    1.5.2   & Dec 18, 2024  & No changes                                  \\ \bottomrule
\end{tabular}

\newpage
\printglossary[type=\acronymtype]

\chapter{Introduction}

\section{Purpose}

The purpose of this document is to identify and create a complete System Requirements Specifications for implementing SatNOGS COMMS.\@
The analysis shall contain a high level overview of the system components and functions, as well as a complete list of functional and non-functional requirements.

\section{Conventions}

The key words `MUST', `MUST NOT', `REQUIRED', `SHALL', `SHALL NOT', `SHOULD', `SHOULD NOT', `RECOMMENDED', `MAY', and `OPTIONAL' in this document are to be interpreted as described in RFC2119.
However, for readability, these words do not appear in all upper case letters in this specification.

\section{Intended Audience}

The intended audiences of this document are developers who will implement the SatNOGS COMMS solution as well as the European Space Agency and the entities that have signed the Letter of Interest for this development (the latter on an informational capacity).
It is assumed that the reader is familiar with satellite communications in general and more specifically, satellite communication subsystems, both for space and ground segments.
It is also assumed that the reader has some knowledge of radio communications technologies, as well as modulation and decoding schemes and basic knowledge of electronics design.

\section{Scope}

The scope of this SRS is the development of SatNOGS COMMS.\@
SatNOGS COMMS is a versatile telecommunications solution suitable for nano-satellites and Cubesats, operating in UHF and S-band, and featuring tight integration with SatNOGS network.
The scope spans from the space segment transceiver to the ground segment station.

\newpage
\printglossary[type=\acronymtype]

\chapter{Description}

\section{System perspective}

\autoref{fig:system-components} shows a high level overview of SatNOGS COMMS components.
The solution spans both on space and ground segments.
On the space segment, a satellite communications subsystem is specified.
This subsystem is the SatNOGS COMMS transceiver, also referred as  “COMMS transceiver” for simplicity in the rest of the document.
COMMS transceiver consists of radio communications hardware, accompanied by related RF frontend, two communication interfaces for exchanging data with the rest of the satellite subsystems, and a controller.
In addition, a DSP unit is available for additional bulk data processing and cognitive radio functions.
On the ground segment, SatNOGS is used as the solution for management, TC\&C and telemetry data warehouse.
SatNOGS consists of a network of ground stations, a service which coordinates the stations (SatNOGS Network) and databases for storing raw and processed telemetry data.
The operator’s interface is a set of dashboards which gives users access to data warehouse where all telemetry is stored as well as a mission control center allowing operators to telecommand and control the spacecraft.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.85\textwidth]{diagrams/srr-system-components.pdf}
  \caption{SatNOGS COMMS components}\label{fig:system-components}
\end{figure}
\section{System functions}

The high-level function of this system is providing a turnkey radio communications solution for spacecrafts which is tightly integrated with SatNOGS.\@
On the space segment, SatNOGS COMMS transceiver is specified to be capable of concurrent operation on two bands; UHF and S-band.
The transceiver primary functions are modulation and coding, based on CCSDS standard.
This includes QPSK, BPSK, MSK and FSK modulating and demodulating functions.
An I/Q interface is also specified for COMMS transceiver.
This interface is used in conjunction with Cognitive Radio capabilities which are included as secondary functions of COMMS transceiver.
Antenna deploying function is also included as a secondary function.
On the ground segment, SatNOGS COMMS ground station is also specified to be capable to receive and transmit on the respective bands, modulations and coding schemes that the COMMS transceiver supports.
In addition to that, SatNOGS COMMS ground station is fully integrated to SatNOGS, including TC\&C functions.
SatNOGS provides the functions for mission control and telemetry storing and viewing through a set of dashboards which can be configured per customer and/or mission.

\chapter{Context}

\section{Actors}

\subsubsection{Satellite Operator}

Satellite Operator, or Operator in the next `Use Cases' session, is the person responsible for running communication, monitoring and housekeeping operations of a satellite mission.

\subsubsection{Satellite Integrator}

Satellite Integrator, or Integrator in the next `Use Cases' session, is the person responsible for designing and building the satellite.

\section{Use Cases}

\subsubsection{Operator via SatNOGS Network communicates with satellite over \acrshort{uhf} band}

The operator, using a UI to connect with ground stations of SatNOGS Network, communicates with the satellite over \acrshort{uhf} band.
Each communication is performed on configured \acrshort{uhf} frequency with one of \acrshort{fsk}, \acrshort{msk} or \acrshort{bpsk} modulations, following a configured telecommunication schema and compensating expected Doppler shift.
%% REQ-013 - CCSDS API for transmission and reception
%% REQ-019 - Support for half duplex communication on UHF of COMMS transceiver
%% REQ-022 - Support for I/Q data streaming
%% REQ-027 - Integration of COMMS ground station with SatNOGS
%% REQ-070 - Support for CCSDS modulation and coding schemes on COMMS ground station
%% REQ-080 - Support for FSK modulation
%% REQ-081 - Support for MSK modulation
%% REQ-082 - Support for BPSK modulation
%% REQ-066 - Configurability of operating frequency of COMMS transceiver
%% REQ-064 - Telemetry application programming interface of COMMS ground station
%% REQ-065 - Telecommand application programming interface of COMMS ground station
%% REQ-143 - Doppler tracking performance

\subsubsection{Operator via SatNOGS Network communicates with the satellite over S-band}

The operator, using a UI to connect with ground stations of SatNOGS Network, communicates with the satellite over S-band.
Each communication is performed on configured S-band frequency with one of \acrshort{msk}, \acrshort{bpsk} or \acrshort{qpsk} modulations, following a configured telecommunication schema and compensating expected Doppler shift.
%% REQ-013 - CCSDS API for transmission and reception
%% REQ-020 - Support for half duplex communication on S-band on COMMS transceiver
%% REQ-022 - Support for I/Q data streaming
%% REQ-027 - Integration of COMMS ground station with SatNOGS
%% REQ-070 - Support for CCSDS modulation and coding schemes on COMMS ground station
%% REQ-081 - Support for MSK modulation
%% REQ-082 - Support for BPSK modulation
%% REQ-083 - Support for \acrshort{qpsk} modulation
%% REQ-066 - Configurability of operating frequency of COMMS transceiver
%% REQ-064 - Telemetry application programming interface of COMMS ground station
%% REQ-065 - Telecommand application programming interface of COMMS ground station
%% REQ-084 - High speed interconnection link of COMMS transceiver
%% REQ-143 - Doppler tracking performance

\subsubsection{Operator via the SatNOGS Network communicates with satellite over \acrshort{uhf} and S-band}

The operator, using a UI to connect with ground stations of SatNOGS Network, communicates with the satellite over \acrshort{uhf} and S-band simultaneously.
Each communication is performed on configured \acrshort{uhf} or S-band frequency with one of \acrshort{fsk} (only for \acrshort{uhf}), \acrshort{msk}, \acrshort{bpsk} or \acrshort{qpsk} (only for S-band) modulations, following a configured telecommunication schema and compensating expected Doppler shift.
%% REQ-013 - CCSDS API for transmission and reception
%% REQ-019 - Support for half duplex communication on UHF of COMMS transceiver
%% REQ-020 - Support for half duplex communication on S-band on COMMS transceiver
%% REQ-021 - Concurrent UHF and S-band operation
%% REQ-022 - Support for I/Q data streaming
%% REQ-027 - Integration of COMMS ground station with SatNOGS
%% REQ-070 - Support for CCSDS modulation and coding schemes on COMMS ground station
%% REQ-080 - Support for FSK modulation
%% REQ-081 - Support for MSK modulation
%% REQ-082 - Support for BPSK modulation
%% REQ-083 - Support for \acrshort{qpsk} modulation
%% REQ-066 - Configurability of operating frequency of COMMS transceiver
%% REQ-064 - Telemetry application programming interface of COMMS ground station
%% REQ-065 - Telecommand application programming interface of COMMS ground station
%% REQ-084 - High speed interconnection link of COMMS transceiver
%% REQ-143 - Doppler tracking performance

\subsubsection{Operator monitors and analyses satellite telemetry data via configurable SatNOGS Dashboard}

Operator, using a configured SatNOGS Dashboard to visualize telemetry data in the form of time series, monitors and analyze satellite telemetry in almost real-time.
%% REQ-045 - Storing of telemetry data in a time series database
%% REQ-046 - Graphical visualization of telemetry data
%% REQ-075 - Configurability of Telemetry Dashboard

\subsubsection{Operator configures frequency and modulation of COMMS transceiver \acrshort{rx} and \acrshort{tx}}

The operator can configure the operating frequency and modulation of the COMMS transceiver for receiving and transmitting independently for both \acrshort{uhf} and S-band.
For \acrshort{uhf}, frequency is in 395--500 MHz frequency range and modulation is one of \acrshort{fsk}, \acrshort{msk} or \acrshort{bpsk} for both \acrshort{tx} and \acrshort{rx}.
For S-band, frequency is in 2200--2290 MHz or 2400--2450 MHz frequency ranges for the \acrshort{tx} and in 2025--2110 MHz, 2200--2290 MHz or 2400--2450 MHz frequency ranges for the \acrshort{rx} and modulation is one of \acrshort{msk}, \acrshort{bpsk} or \acrshort{qpsk} for both \acrshort{tx} and \acrshort{rx}.
%% REQ-066 - Configurability of operating frequency of COMMS transceiver
%% REQ-072 - Configurability of TX modulation of COMMS transceiver
%% REQ-074 - Configurability of RX modulation of COMMS transceiver

\subsubsection{Operator configures frequency and modulation of ground station \acrshort{rx} and \acrshort{tx}}

The operator can configure the operating frequency and modulation of the ground station for receiving and transmitting independently for both \acrshort{uhf} and S-band.
For \acrshort{uhf}, frequency is in 395--500 MHz frequency range and modulation is one of \acrshort{fsk}, \acrshort{msk} or \acrshort{bpsk} for both \acrshort{tx} and \acrshort{rx}.
For the S-band, frequency is in 2200--2290 MHz or 2400--2450 MHz frequency ranges for the \acrshort{tx} and in 2025--2110 MHz, 2200--2290 MHz or 2400--2450 MHz frequency ranges for the \acrshort{rx} and modulation is one of \acrshort{msk}, \acrshort{bpsk} or \acrshort{qpsk} for both \acrshort{tx} and \acrshort{rx}.
%% REQ-067 - Configurability of operating frequency of COMMS ground station
%% REQ-071 - Configurability of TX modulation of COMMS ground station
%% REQ-073 - Configurability of RX modulation of COMMS ground station

\subsubsection{Operator configures power of COMMS transceiver transmission}

The operator can configure the transmission power of the COMMS transceiver to at least 30dBm.
%% REQ-068 - Configurability of TX power of COMMS transceiver

\subsubsection{Operator deploys antenna over \acrshort{uhf} band}

The operator deploys satellite antenna by sending telecommands via SatNOGS Network over \acrshort{uhf} band.
Deployment is confirmed through telemetry.
%% REQ-015 - Control of antenna deployment mechanism
%% REQ-062 - Antenna deployment electrical interface of COMMS transceiver

\subsubsection{Operator deploys antenna over S-band}

The operator deploys a satellite antenna by sending telecommands via SatNOGS Network over S-band.
Deployment is confirmed through telemetry.
%% REQ-015 - Control of antenna deployment mechanism
%% REQ-062 - Antenna deployment electrical interface of COMMS transceiver

\subsubsection{Operator shuts down COMMS transceiver \acrshort{uhf} subsystem over \acrshort{uhf} or S-band permanently}

Operator shuts down COMMS transceiver subsystem responsible for \acrshort{uhf} transmissions by sending telecommands via SatNOGS Network either over \acrshort{uhf} or S-band.
%% REQ-009 - Support for ground segment initiated shutdown of COMMS transceiver
%% REQ-010 - Persistence of ground segment initiated shutdown of COMMS transceiver

\subsubsection{Operator shuts down COMMS transceiver S-band subsystem over \acrshort{uhf} or S-band permanently}

The operator shuts down the COMMS transceiver subsystem responsible for S-band transmissions by sending telecommands via SatNOGS Network either over \acrshort{uhf} or S-band.
%% REQ-009 - Support for ground segment initiated shutdown of COMMS transceiver
%% REQ-010 - Persistence of ground segment initiated shutdown of COMMS transceiver

\subsubsection{Integrator configures initial \acrshort{uhf} frequency and modulation of COMMS transceiver}

The integrator sets the initial operating frequency and modulation of the COMMS transceiver for receiving and transmitting independently for \acrshort{uhf} band.
Frequency is in 395--500 MHz frequency range, and modulation is one of \acrshort{fsk}, \acrshort{msk} or \acrshort{bpsk} for both \acrshort{tx} and \acrshort{rx}.
%% REQ-136 - UHF radio frequency tunability of COMMS transceiver
%% REQ-066 - Configurability of operating frequency of COMMS transceiver

\subsubsection{Integrator configures initial S-band frequency and modulation of COMMS transceiver}

The integrator sets the initial operating frequency and modulation of the COMMS transceiver for receiving and transmitting independently for S-band.
Frequency is in 2200--2290 MHz or 2400--2450 MHz frequency ranges for the \acrshort{tx} and in 2025--2110 MHz, 2200--2290 MHz or 2400--2450 MHz frequency ranges for the \acrshort{rx} and modulation is one of \acrshort{msk}, \acrshort{bpsk} or \acrshort{qpsk} for both \acrshort{tx} and \acrshort{rx}.
%% REQ-137 - S-band radio frequency tunability of COMMS transceiver
%% REQ-066 - Configurability of operating frequency of COMMS transceiver

\subsubsection{Integrator interconnects COMMS transceiver with other satellite subsystems}

Integrator interconnects COMMS transceiver with other satellite subsystems.
The interconnection with the subsystems is achieved through the interface connector, the antenna deployment electrical interface and the power supply electrical interface.
Needed communication is achieved through \acrshort{can} Bus protocol and/or SPI\@.
%% REQ-006 - Communication interface protocol of COMMS transceiver
%% REQ-007 - Subsystem interface connector of COMMS transceiver
%% REQ-017 - Power supply electrical interface of COMMS transceiver
%% REQ-062 - Antenna deployment electrical interface of COMMS transceiver
%% REQ-153 - External reset mechanism

\subsubsection{Integrator integrates physically COMMS transceiver with Satellite}

Integrator integrates physically COMMS transceiver with Satellite via its mechanical interface and its calculated mass.
%% REQ-008 - Mechanical interface of COMMS transceiver
%% REQ-063 - Maximum mass of COMMS transceiver

\subsubsection{Integrator configures antenna deployment signal timing and signal type}

Integrator configures signal timing and signal type for antenna deployment through the antenna deployment electrical interface.

\subsubsection{Integrator configures antenna deployment handling from COMMS}

Integrator configures antenna deployment to be handled by COMMS Software.

\subsubsection{Integrator configures antenna deployment handling from subsystem}

Integrator configures antenna deployment to be handled by another subsystem like OBC\@.
COMMS responds on antenna deploy command and performs deployment.
%% REQ-015 - Control of antenna deployment mechanism
%% REQ-149 - Configurability of antenna deployment signal timing
%% REQ-151 - Configurability of antenna deployment signal type
%% REQs without use case:
%% Functional:
%% REQ-026 - Cognitive Radio capabilities
%% REQ-139 - Single event mitigation techniques
%% Interface:
%% REQ-018 - RF connector interface of COMMS transceiver
%% Environmental:
%% REQ-035 - Operational and non operational temperature of COMMS transceiver
%% REQ-076 - Operation in vacuum conditions
%% REQ-079 - Electromagnetic compatibility
%% REQ-085 - Mechanical stress and vibration
%% Logistics Support Requirements
%% REQ-059 - Product documentation
%% Product Assurance Requirements
%% REQ-030 - Availability of components
%% REQ-031 - Product store environment
%% REQ-033 - Hardware components reliability
%% REQ-034 - Control of assembly procedure
%% REQ-036 - Maintenance-free COMMS transceiver hardware
%% REQ-037 - Software maintainability of COMMS transceiver
%% REQ-042 - Software reliability of COMMS transceiver
%% Design Requirements:
%% REQ-038 - Evaluation of critical hardware components characteristics of COMMS transceiver
%% REQ-043 - Impedance of RF components and transmission lines of COMMS transceiver
%% REQ-078 - Design provisions for EMC control of COMMS transceiver
%% REQ-092 - Modular design in COMMS transceiver hardware
%% REQ-140 - Temperature sensors
%% REQ-150 - Recovery mechanisms from failures
%% REQ-152 - Internal reset mechanism
%% Verification Requirements:
%% REQ-049 - Realistic conditions in Telemetry communication testing
%% REQ-050 - Realistic conditions in TC&C testing
%% REQ-051 - Software testing and verification
%% REQ-052 - Thermal vacuum cycling test and verification of COMMS transceiver
%% REQ-053 - Resonance survey test and verification of COMMS transceiver
%% REQ-054 - Sinusoidal vibration test and verification of COMMS transceiver
%% REQ-055 - Random vibration test and verification of COMMS transceiver
%% REQ-056 - Shock test and verification of COMMS transceiver
%% REQ-057 - EMC test and verification of COMMS transceiver
%% REQ-058 - Conditions, tolerances and accuracies of the tests
%% REQ-086 - Coverage and pass ratio for unit testing of COMMS transceiver
%% REQ-087 - Coverage and pass ratio for functional testing of COMMS transceiver
%% REQ-088 - Coverage and pass ratio for E2E testing of COMMS transceiver
%% REQ-089 - Coverage and pass ratio for unit testing of COMMS ground segment
%% REQ-090 - Coverage and pass ratio for functional testing of COMMS ground segment
%% REQ-091 - Coverage and pass ratio for E2E testing of COMMS ground segment
%% Performance Requirements:
%% REQ-029 - Frequency reference crystal stability
%% REQ-032 - Maximum permissible spurious emissions
%% REQ-039 - Minimum UHF data rate of COMMS transceiver
%% REQ-040 - Minimum S-band data rate of COMMS transceiver
%% REQ-041 - Minimum TX power capabilities of COMMS transceiver
%% REQ-061 - RF operational stability of COMMS transceiver
%% REQ-138 - Transmitted signal quality
%% REQ-142 - Ground station performance of demodulation and decoding
%% REQ-144 - COMMS performance of demodulation and decoding
%% REQ-145 - Power consumption in UHF band, RX mode
%% REQ-146 - Power consumption in UHF band, TX mode
%% REQ-147 - Power consumption in S band, RX mode
%% REQ-148 - Power consumption in S band, TX mode

\chapter{Requirements}

\IfFileExists{include/requirements}{\input{include/requirements}}{} % chktex 27

\section{Functional Requirements}

Functional requirements define the functions that the SatNOGS COMMS solution shall provide in order to meet the requirements of the satellite operator and a Cubesat mission. This section provides the functional requirements for both the ground and the space segment.

\ifdefined\Functional{}
\Functional{}
\fi
\clearpage

\section{Interface Requirements}

This section contains requirements which are related to the interconnection or relationship characteristics between the product and other items.

\ifdefined\Interface{}
\Interface{}
\fi
\clearpage

\section{Environmental Requirements}

This section contains requirements which are related to a product or the system environment during its life cycle.

\ifdefined\Environmental{}
\Environmental{}
\fi
\clearpage

\section{Operational Requirements}

This section contains requirements which are related to the system operability.

\ifdefined\Operational{}
\Operational{}
\fi
\clearpage

\section{Logistics Support Requirements}

This section contains requirements related to logistics support considerations to ensure the effective and economical support of a system for its life cycle.

\ifdefined\LogisticsSupport{}
\LogisticsSupport{}
\fi
\clearpage

\section{Product Assurance Requirements}

This section contains requirements which are related to the relevant activities covered by the product assurance in order to ensure the quality of the product.

\ifdefined\ProductAssurance{}
\ProductAssurance{}
\fi
\clearpage

\section{Configuration Requirements}

This section contains requirements which are related to the composition of the product or its organization.

\ifdefined\Configuration{}
\Configuration{}
\fi
\clearpage

\section{Design Requirements}

This section contains requirements which are related to the imposed design and construction standards such as design standards, selection list of components or materials, interchangeability, safety or margins.

\ifdefined\Design{}
\Design{}
\fi
\clearpage

\section{Verification Requirements}

This section contains requirements which are related to the imposed verification methods, such as compliance to verification standards, usage of test methods or facilities.

\ifdefined\Verification{}
\Verification{}
\fi
\clearpage

\section{Performance Requirements}

This section contains requirements which define the minimum acceptable performance characteristics that the system shall fulfill under all possible nominal operation scenarios, providing a predefined and predictable operation.

\ifdefined\Performance{}
\Performance{}
\fi
\clearpage

\end{document}
