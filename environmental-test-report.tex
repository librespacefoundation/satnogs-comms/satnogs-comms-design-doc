%% SatNOGS COMMS - Environmental Test Report
%% Copyright (C) 2023, 2024 Libre Space Foundation
%%
%% This work is licensed under a
%% Creative Commons Attribution-ShareAlike 4.0 International License.
%%
%% You should have received a copy of the license along with this
%% work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.

% Linter settings
% chktex-file 35

\documentclass[english,title,a4paper]{report}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{float}
\usepackage[hidelinks]{hyperref}
\usepackage[toc,acronyms,section,nopostdot,nonumberlist,section=section,numberedsection]{glossaries}
\usepackage[toc]{appendix}
\usepackage{graphicx}
\usepackage{lastpage}
\usepackage{titlesec}
\usepackage{fancyhdr}
\usepackage{lipsum}
\usepackage{listings}
\usepackage{booktabs}
\usepackage[maxfloats=500]{morefloats}
\usepackage{lscape}
\usepackage{makecell}
\usepackage[table]{xcolor}
\usepackage{fixme}
\usepackage{pdfpages}
\usepackage{subcaption}

\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=black,
    filecolor=magenta,
    urlcolor=blue,
    citecolor=black,
}

\fancypagestyle{plain}{
  \fancyhf{}
  \fancyhead[L]{\includegraphics[width=1.5cm]{diagrams/LSF_HD_Icon_Color.png}} %chktex-file 25
  \fancyhead[R]{SatNOGS COMMS\\Environmental Test Report}
  \fancyfoot[L]{Version \version}
  \fancyfoot[C]{\thepage{} of~\pageref{LastPage}}
  \fancyfoot[R]{\copyright{} Libre Space Foundation}
  \renewcommand{\headrulewidth}{0.4pt}
  \renewcommand{\footrulewidth}{0.4pt}
}
\pagestyle{plain}
\setcounter{secnumdepth}{3}
\setcounter{tocdepth}{3}

\input{include/version}

\makeglossaries{}

%% Acronyms
\input{glossaries/acronyms}

\title{
  Environmental Test Report \\
  \large{SatNOGS COMMS}
}
\author{\includegraphics[scale=0.2]{diagrams/lsf-logo.png}}
\date{Version \version}

\titleformat{\chapter}[hang]
{\normalfont\bfseries\Huge}{\thechapter.}{10pt}{}

\begin{document}

\hypersetup{pageanchor=false}
\maketitle
\hypersetup{pageanchor=true}
\tableofcontents

\chapter{Changelog}

\begin{tabular}{llp{6.5cm}}
    \toprule
    Version & Date          & Changes                                       \\ \midrule
    1.1     & Dec 29, 2023  & Initial release                               \\ \midrule
    1.2     & Mar 19, 2024  & No Changes                                    \\ \midrule
    1.3     & Apr 30, 2024  & No Changes                                    \\ \midrule
    1.4     & Nov 05, 2024  & Add appendix about Omitting Vibration and
                              TVAC Tests for Minor Updates in SatNOGS COMMS \\ \midrule
    1.5     & Dec 10, 2024  & No Changes                                    \\ \midrule
    1.5.1   & Dec 10, 2024  & Fix static GitLab pipeline                    \\ \midrule
    1.5.2   & Dec 18, 2024  & Add the status of FPGA during TVAC test       \\ \bottomrule
\end{tabular}

\newpage
\printglossary[type=\acronymtype]

\chapter{Introduction}

This document outlines the procedures and results obtained in the thermal vacuum and vibration tests carried out to qualification levels on the SatNOGS COMMS Board.

\chapter{Thermal-Vacuum Testing}

\section{Facility}

Thermal Vacuum testing of the \acrshort{dut} was carried out in the NanoSat Lab at Technical University of Catalonia  under the control and supervision of facility technicians in the following chamber:

\begin{figure}[H]
  \centering
  \includegraphics[width=1\textwidth]{diagrams/tvac-specs-upc.png}
  \caption{TVAC Specification}\label{fig:tvac-specs-upc}
\end{figure}

\section{DUT and Thermocouple Placement}
A total of 5 Type-K thermocouples were used to measure DUT temperature, these were insulated from the DUT with a layer of Kapton and covered in a layer of Kapton.
\begin{table}[!ht]
    \centering
    \begin{tabular}{ll}
    \toprule
        Test point & Description \\ \midrule
        TP-1 & UHF PA in PCB \\ \midrule
        TP-2 & Middle of EMI Shield \\ \midrule
        TP-3 & FPGA in EMI Shield \\ \midrule
        TP-4 & PC104 FlatSat \\ \midrule
        TP-5 & Control probe \\ \bottomrule
    \end{tabular}
    \caption{Thermocouples placement}\label{table:k-type-tp}
\end{table}

\begin{figure}[H]
  \centering
  \includegraphics[width=1\textwidth]{diagrams/tvac-tp-1.png}
  \caption{Test point 1}\label{fig:tvac-tp-1}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=1\textwidth]{diagrams/tvac-tp-all.png}
  \caption{Test point 2--5}\label{fig:tvac-tp-all}
\end{figure}

A temperature level is deemed to be reached when the probe TP-5 have achieved said temperature.

Thermal isolation from the thermal shroud and thermal table was achieved using PC104 FlatSat, thus eliminating conduction to the chamber.

\section{Planned Test Profile}

4 full TVAC cycles ranging from -20$^{\circ}$C to 50$^{\circ}$C are to be carried out on the SatNOGS COMMS PCB in order to ensure the non-structural alteration of the unit.
The minimum pressure established was of 10$^{-5}$mbar before activating the thermal shroud.

More details in \url{https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-verification/-/issues/53}

\section{Test Results}

Total TVAC cycling time of the test was 12 hours, 1 cycles.
Seen below is the temperature gradient of the previously mentioned thermocouples on the \acrshort{dut}:

\begin{figure}[H]
  \centering
  \includegraphics[width=1\textwidth]{diagrams/tvac-measurements.png}
  \caption{TVAC Cycling}\label{fig:tvac-measurements}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=1\textwidth]{diagrams/pre-tvac.jpg}
  \caption{Pre TVAC Bottom Side of SatNOGS COMMS PCB}\label{fig:pre-tvac<s}
\end{figure}

A maximum temperature of 61.0$^{\circ}$C was reached on TP-4 (PC104 FlatSat) and a minimum temperature of -28.6$^{\circ}$C was reached on TP-4 (PC104 FlatSat).
The average temperature across all temperature probes of the DUT always reached the limit established.
The vacuum level of the chamber throughout the test, always complying with our minimum requirement of 10$^{-5}$mbar.
After visual inspection in each axis tests there are no any signs of damage.

\begin{figure}[H]
  \centering
  \includegraphics[width=1\textwidth]{diagrams/post-tvac.jpg}
  \caption{Post TVAC Bottom Side of SatNOGS COMMS PCB}\label{fig:post-tvac}
\end{figure}

\subsection{Functional Test Results}
Throughout the entire test duration the board had been instructed to report periodically basic health-status telemetry to the control PC via the CAN-bus interface.
The FPGA module is always enabled without running anything.
No loss of communication or any abnormal resets or power-cycles has been identified.

In addition, in selected phases of the thermal cycling (cold and hot plateau), some extreme spacial cases of the functional tests were performed.
The board was instructed to transmit consecutive frames with a bandwidth of 50 kHz, at 1W of peak power for 60 seconds with a duty cycle of around 90\%.
The goal of these stress tests is to identify the heat dissipation characteristics of the board, and spot any qualitative variations in the transmitted signal.
As can been seen from \autoref{fig:tvac-functional}, the frequency variation throughout the entire stress test duration was negligible (<100 Hz) and the spectrum mask did not exhibit any noticeable change.
Moreover, \autoref{fig:hot-palteau} and \autoref{fig:cold-plateau} show the temperature readings during the stress tests at the hot and cold plateaus respectively, as the recorded by the on-board temperature sensors.
The results reveal that the SatNOGS-COMMS is able to dissipate effectively the produced heat and continue to operate nominally in both high and low temperature scenarios.

\begin{figure}
    \begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[width=1.0\textwidth]{diagrams/psd1.png}
    \end{subfigure}
        \begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[width=1.0\textwidth]{diagrams/psd2.png}
    \end{subfigure}
    \caption{A 35 secs transmission on the UHF at the hot plateau}\label{fig:tvac-functional}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=0.95\linewidth]{diagrams/hot-plateau-temp.png}
    \caption{On-board temperature sensor readings during the stress test at hot plateau}\label{fig:hot-palteau}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=0.95\linewidth]{diagrams/cold-plateau-temp.png}
    \caption{On-board temperature sensor readings during the stress test at cold plateau}\label{fig:cold-plateau}
\end{figure}

\chapter{Vibration Test Report}

\section{Facility}

Vibration testing campaign of the DUT was carried out in the NANOSAT LAB at Technical University of Catalonia under the control and supervision of facility technicians in the V400 DataPhysics shaker system with the following characteristics:

\begin{figure}[H]
  \centering
  \includegraphics[width=0.6\textwidth]{diagrams/shaker-specs.png}
  \caption{V400 DataPhysics shaker Specifications}\label{fig:shaker-specs}
\end{figure}
The table below summarises the main characteristics of the acceleration sensors:
\begin{table}[!ht]
    \centering
    \begin{tabular}{llllll}
    \toprule
        Part & Manufacturer & Type & Range & Sensitivity & Reference \\ \midrule
        8702B500 & Kistler & Mono-Axial & $\pm500g$ & $10.25mV/g$ & Green \\ \midrule
        8640A50 & Kistler & Mono-Axial & $\pm50g$ & $100mV/g$ & Red   \\ \bottomrule
    \end{tabular}
    \caption{Acceleration Sensors Specifications}\label{table:accelerometers}
\end{table}

\section{DUT and Accelerometer Placement}

Two different adapter plates were used to mount the \acrshort{dut} in vibration table. One for X-Y configuration and one for Z.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.6\textwidth]{diagrams/vib-y-axis.png}
  \caption{Y axis accelerometers position, }\label{fig:vib-y-axis}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.6\textwidth]{diagrams/vib-x-axis.png}
  \caption{X axis accelerometers position}\label{fig:vib-x-axis}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.6\textwidth]{diagrams/vib-z-axis.png}
  \caption{Z axis accelerometer position}\label{fig:vib-z-axis}
\end{figure}

\section{Planned Test Profile and Sequence}
The SatNOGS COMMS PCB has been tested at qualification levels as specified in ECSS-E-ST-10--03C.
\begin{table}[!ht]
    \centering
    \begin{tabular}{ll}
    \toprule
        Frequency (Hz) & Level (g) \\ \midrule
        5--2000 & 0.4 \\ \bottomrule
    \end{tabular}
    \caption{Resonance Survey Profile}\label{table:survey-profile}
\end{table}

\begin{table}[!ht]
    \centering
    \begin{tabular}{lll}
    \toprule
        Frequency (Hz) & Level (g) & Sweep Rate (oct/min) \\ \midrule
        5--100 & 2.5 & 2 \\ \midrule
        100--125 & 1.25 & 2 \\ \bottomrule
    \end{tabular}
    \caption{Sine Vibration}\label{table:sine-profile}
\end{table}

\begin{table}[!ht]
    \centering
    \begin{tabular}{lll}
    \toprule
        Frequency (Hz) & Level (g) & Full Cycles \\ \midrule
        50 & 9.6 & 6 \\ \bottomrule
    \end{tabular}
    \caption{Quasi Static Shock}\label{table:quasi-profile}
\end{table}

\begin{table}[!ht]
    \centering
    \begin{tabular}{llll}
    \toprule
        Frequency (Hz) & Level (g$^{2}$/Hz) & Overall ($g*rms$) & Test Time (s) \\ \midrule
        20 & 0.006 & 10 & 120 \\ \midrule
        20--100 & 0.006 + 6 dB/oct & 10 & 120 \\ \midrule
        100--700 & 0.04 & 10 & 120 \\ \midrule
        700--2000 & 0.04--6 dB/oct & 10 & 120 \\ \midrule
        2000 & 0.006 & 10 & 120 \\ \bottomrule
    \end{tabular}
    \caption{Random Vibration}\label{table:random-profile}
\end{table}

At the beginning of each axis test a resonance survey is conducted in adapter.
Following the sequence of test in each axis:
\begin{enumerate}
  \item Resonance Survey
  \item Sine Vibration
  \item Quasi Static Shock
  \item Random Vibration
  \item Resonance Survey
\end{enumerate}

\section{Test Results}

The lowest resonance frequencies of the SatNOGS COMMS are summarised in the following table (accelerometer sensor in the \acrshort{dut}):
\begin{table}[!ht]
    \centering
    \begin{tabular}{llllll}
    \toprule
        Sensor &  & Pre Survey &  & Post Survey &  \\ \midrule
         &  & Freq. (Hz) & Ampl.\ (g) & Freq. (Hz) & Ampl.\ (g) \\ \midrule
        X & Resonance 1 & 1.09k & 3.43 & 1.09k & 3.41 \\
         & Resonance 2 & 1.13k & 3.54 & 1.13k & 3.66 \\
         & Resonance 3 & 1.30k & 8.31 & 1.30k & 8.69 \\ \bottomrule
        Y & Resonance 1 & 1.10k & 2.81 & 1.10k & 2.68 \\
         & Resonance 2 & 1.14k & 2.79 & 1.14k & 2.84 \\
         & Resonance 3 & 1.27k & 3.35 & 1.27k & 3.18 \\
         & Resonance 4 & 1.39k & 13.3 & 1.42k & 9.23 \\
         & Resonance 5 & 1.46k & 5.75 & N/A & N/A \\ \bottomrule
        Z & Resonance 1 & 1.07k & 7.62 & 1.07k & 10.1 \\
         & Resonance 2 & 1.17k & 179m & 1.17k & 161m \\
         & Resonance 3 & 1.33k & 5.60 & 1.33k & 5.73 \\ \midrule
    \end{tabular}
    \caption{Resonance Frequencies}\label{table:vib-results}
\end{table}

After visual inspection in each axis tests there are no any signs of damage.
In the following link you can find more photos: \url{https://cloud.libre.space/s/KeqsBpEyfSMjnQQ}
Also, in this link there are plots results: \url{https://cloud.libre.space/s/yen62QiJwqbf2ZY}

\subsection{Functional Test Results}

After each axis vibration test, a simple health status request of the board was instructed through the CAN-Bus.
Additionally, multiple FSK-2 50 kHz frame transmissions at 1W peak power were conducted on both bands.
Both the health status telemetry and the spectrum masks of the transmissions did not reveal any noticeable effect of the vibration test on the SatNOGS-COMMS board.

\chapter{Conclusion}

The SatNOGS COMMS PCB is deemed to have passed the qualification campaign established.
The \acrshort{dut} does not present any electrical, physical or structural deformations between before and after having completed the tests and performed nominally, being demonstrated by the functional tests in TVAC environment.

\begin{appendices}
\chapter{Omitting Vibration and TVAC Tests for Minor Updates in SatNOGS COMMS}

As our project progresses, we've determined that vibration and thermal vacuum (TVAC) tests are not necessary for new versions due to the minimal nature of the changes.
This decision is based on several key factors.
Firstly, the mounting points of subsystems within the CubeSat remain unchanged, ensuring consistent structural integrity during testing.
Additionally, the mounting points of shield onto the printed circuit board (PCB) haven't been modified, maintaining the overall mechanical stability of the design.
While there are some visual changes apparent in the provided photograph, these cosmetic alterations don't impact the functional performance of the spacecraft \autoref{fig:comms-dif}.
Furthermore, the placement of large integrated circuits (ICs) hasn't changed, which is crucial for thermal management during TVAC testing.
Given these considerations, we conclude that the modifications do not significantly affect the subsystem's thermal performance during TVAC testing.
Therefore, we can confidently proceed with the development cycle without the need for extensive vibration and TVAC testing at this stage.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.6\textwidth]{diagrams/comms-dif.png}
  \caption{SatNOGS COMMS v0.2.0 (left) and v0.3.1 (right)}\label{fig:comms-dif}
\end{figure}

\end{appendices}

\end{document}
